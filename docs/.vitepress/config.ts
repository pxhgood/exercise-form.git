import type { UserConfig } from "vitepress";

export const config: UserConfig = {
  title: "Exercise Form",
  description: "a Vue 3 based component library for designers and developers",
  lastUpdated: true,
  base: "/exercise-form-docs.github.io/",
  head: [
    [
      "link",
      { rel: "icon", href: "/exercise-form-docs.github.io/images/logo.svg" }
    ],
    [
      "meat",
      { rel: "icon", href: "/exercise-form-docs.github.io/images/logo.svg" }
    ],
    [
      "link",
      {
        rel: "stylesheet",
        href: "https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.css"
      }
    ],
    [
      "script",
      {
        src: "https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"
      }
    ]
  ],
  themeConfig: {
    logo: "/images/logo.svg",
    footer: {
      message: "Released under the MIT License.",
      copyright: "Copyright © 2023-PRESENT"
    },
    search: {
      center: "right",
      provider: "local", // 可以开启本地搜索
      // provider: "algolia",
      options: {
        // appId: "PPE3KXB4ZS",
        // apiKey: "15a9e3babd03b811492c75273937e54d",
        // indexName: "exercise-form",
        placeholder: "请输入关键词",
        translations: {
          button: {
            buttonText: "请输入关键词"
          }
        }
      }
    },
    outline: {
      label: "本页目录"
    },
    editLink: {
      pattern: "https://gitee.com/pxhgood/exercise-form/blob/dev/docs/:path",
      text: "为此页提供修改建议"
    },
    lastUpdated: {
      text: "上次更新"
    },
    docFooter: {
      prev: "上一篇",
      next: "下一篇"
    },
    nav: [
      { text: "指南", link: "/guide/design" },
      { text: "组件", link: "/components/designer" },
      { text: "使用手册", link: "/manual/m_02" }
      // {
      //   text: "V0.0.0-dev.1",
      //   items: [
      //     {
      //       text: "版本发布",
      //       link: "https://gitee.com/pxhgood/exercise-form.git"
      //     }
      //   ]
      // }
    ],
    socialLinks: [
      { icon: "github", link: "https://gitee.com/pxhgood/exercise-form.git" }
    ],
    sidebar: {
      "/guide/": [
        {
          text: "指南",
          items: [
            {
              text: "Form Designer",
              link: "/components/designer"
            },
            {
              text: "Form Render",
              link: "/components/render"
            }
          ]
        }
      ],
      "/components/": [
        {
          text: "组件",
          items: [
            {
              text: "Form Designer",
              link: "/components/designer"
            },
            {
              text: "Form Render",
              link: "/components/render"
            }
          ]
        }
      ],
      "/manual/": [
        {
          text: "使用手册",
          items: [
            // {
            //   text: "后端集成",
            //   link: "/manual/m_01"
            // },
            {
              text: "数据源",
              link: "/manual/m_02"
            },
            {
              text: "表单及表单控制",
              link: "/manual/m_03"
            },
            {
              text: "表单组件属性设置",
              link: "/manual/m_04"
            },
            {
              text: "栅格布局和表格布局",
              link: "/manual/m_05"
            },
            {
              text: "数据表格",
              link: "/manual/m_06"
            },
            {
              text: "树形和对象容器",
              link: "/manual/m_07"
            },
            {
              text: "弹框和抽屉",
              link: "/manual/m_08"
            },
            {
              text: "单行子表单和多行子表单",
              link: "/manual/m_09"
            },
            {
              text: "上传图片和文件",
              link: "/manual/m_10"
            },
            {
              text: "自定义CSS",
              link: "/manual/m_11"
            },
            {
              text: "自定义函数",
              link: "/manual/m_12"
            },
            {
              text: "组件暴露方法使用",
              link: "/manual/m_13"
            },
            {
              text: "表单校验使用",
              link: "/manual/m_14"
            }
          ]
        }
      ]
    }
  }
};

export default config;
