import DefaultTheme from "vitepress/theme";
import "./custom.scss";
import mdItCustomAttrs from "markdown-it-custom-attrs";

export default {
  ...DefaultTheme,
  NotFound: () => "404", // <- this is a Vue 3 functional component
  enhanceApp({ app, router, siteData }) {},
  markdown: {
    config: (md) => {
      md.use(mdItCustomAttrs, "image", {
        "data-fancybox": "gallery"
      });
    }
  }
};
