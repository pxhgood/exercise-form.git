# 表单渲染器

## 基础用法

```vue
<template>
  <ex-form-render :form-json="formJson" :form-data="formData" />
</template>
<script lang="ts" setup>
import { ref, reactive } from "vue";
import { DesFormParams } from "exercise-form";

const formData = ref({});
const formJson = reactive<DesFormParams>({
  formConfig: {}, //表单设置
  widgetList: [] //组件列表
});
</script>
```

## 属性参数

| 属性名      | 描述         |  类型   | 默认值 |
| ----------- | ------------ | :-----: | :----: |
| `form-json` | 表单JSON数据 | object  |   -    |
| `form-data` | 表单数据     | object  |   -    |
| `disabled`  | 禁用         | boolean | false  |

## 导出属性

| 属性名          | 描述            |   类型    |
| --------------- | --------------- | :-------: |
| `formRenderRef` | 当前表单Ref对象 | Ref\<any> |
| `formData`      | 表单数据        |  object   |

`formRenderRef` 方法，及包含[el-form](https://element-plus.org/zh-CN/component/form.html#form-exposes)导出方法和子组件导出的方法。当前为子组件导出的方法：

| 方法           | 描述              |   类型   |      参数      |
| -------------- | ----------------- | :------: | :------------: |
| `getWidgetRef` | 获取当前组件的Ref | function | 组件名（name） |
