## 基本介绍

Exercise Form是一款vue3低代码表单，基于Element Plus组件库，借鉴[Variant Form](https://vform666.com/)表单。

Exercise Form由表单设计器ExFormDesigner和表单渲染器ExFormRender两部构成，ExFormDesigner通过拖拽组件方式生成JSON格式的表单对象，
ExFormRender负责将表单JSON渲染为Vue组件。

## 项目特点

- :monocle_face: 仅支持Vue3版本，不支持IE 11及以下的版本浏览器
- :monocle_face: 使用TypeScript编写，提供类型接口
- :white_check_mark: 涉及[Element Plus](https://element-plus.org/zh-CN/guide/installation.html)大部分表单组件
- :white_check_mark: 提供组件交互事件
- :white_check_mark: 提供组件属性设置和组件事件方法
- :white_check_mark: 支持自定义CSS和自定义函数
- :white_check_mark: 支持自定义主题设置
- :white_check_mark: 支持\<script setup>模式的单页面组件和弹框代码生成
- :white_check_mark: 提供表单自定义校验
- :white_check_mark: 支持后端交互
- :tada: 提高摸鱼效率

👍[点赞支持，谢谢](https://gitee.com/pxhgood/exercise-form.git)

👉[项目演示地址](https://pxhnice.github.io/exercise-form.github.io/)

## 目录结构

项目采用monorepo进行代码管理，结构如下

```
root
└─ packages
    ├─ components              // 容器组件目录
    |  ├─ code-editor          // 代码编辑器
    |  ├─ form-designer        // 表单设计器
    |  |   └─src
    |  |     ├─ panel
    |  |     |    └─ source    // 数据源
    |  |     └─ widget         // 表单容器
    |  └─ form-render          // 表单渲染器
    ├─ constants               // 存放表单组件配置数据
    ├─ core                    // 处理表单操作构建相关
    ├─ directives              // 自定义指令
    ├─ hooks                   // 钩子
    ├─ icons                   // svg 图标
    ├─ theme                   // 组件样式
    ├─ utils                   // 工具
    └─ widget                  // 子组件目录
      ├─ components
      ├─ designer
      |    ├─ container        // 设计器容器组件
      |    ├─ fields           // 设计器自定义组件
      |    └─ property         // 表单属性对应组件
      └─ render
          └─ container         // 表单渲染容器组件
└─ script
    └─ build                   // 项目打包
```

## 开始安装

> 由于该组件基于Element Plus搭建，使用前请先安装[Element Plus](https://element-plus.org/zh-CN/guide/installation.html)、
> 组件[图标](https://element-plus.org/zh-CN/component/icon.html)，再安装该组件，安装如下。
> 目前项目处于开发阶段仅用于<span style="color:#f36d09">当前测试使用</span>

::: code-group

```bash [npm]
 npm install exercise-form
```

```bash [pnpm]
 pnpm add exercise-form
```

:::

## 开始使用

```ts
//main.ts
import { createApp } from "vue";
// 组件引入
import ElementPlus from "element-plus";
import ExerciseForm from "exercise-form";
// 样式引入
import "element-plus/dist/index.css";
import "exercise-form/dist/index.css";
import * as ElementPlusIconsVue from "@element-plus/icons-vue";
import App from "./App.vue";

const app = createApp(App);

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component);
}

app.use(ElementPlus);
app.use(ExerciseForm);
app.mount("#app");
```
