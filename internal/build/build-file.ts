import path from "path";
import { copyFile, mkdir } from "fs/promises";
import { parallel, series } from "gulp";
import { proPath, buildPath, pkgPath, run, themePath, exPath } from "./src";

export const copyStyle = async () => {
  await mkdir(path.resolve(exPath, "dist"), { recursive: true });
  await copyFile(`${pkgPath}/theme/dist/index.css`, `${exPath}/dist/index.css`);
};

export const copyFiles = () =>
  Promise.all([
    copyFile(
      path.resolve(proPath, "typings", "global.d.ts"),
      path.resolve(exPath, "global.d.ts")
    ),
    copyFile(
      path.resolve(proPath, "README.md"),
      path.resolve(exPath, "README.md")
    )
  ]);

export const buildComponent = async () => {
  await run("pnpm -w run gen:version", `${buildPath}`);
  await run("pnpm run build", `${buildPath}`);
};

export const buildTheme = async () => {
  await run("pnpm -w run build:theme", `${themePath}`);
};

export const buildCss = async () => {
  await buildTheme();
  await copyStyle();
};

export default series(
  parallel(
    () => buildCss(),
    () => buildComponent(),
    copyFiles
  )
);
