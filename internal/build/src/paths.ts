import { resolve } from "path";

export const proPath = resolve(__dirname, "..", "..", "..");

export const pkgPath = resolve(proPath, "packages");
export const docsPath = resolve(proPath, "docs");
export const playPath = resolve(pkgPath, "play");

export const buildPath = resolve(proPath, "internal", "build");

// dist
export const buildOutput = resolve(pkgPath, "dist");
// dist/exercise-form
export const exOutput = resolve(buildOutput, "exercise-form");

export const componentPath = resolve(pkgPath, "components");
export const exPath = resolve(pkgPath, "exercise-form");
export const utilPath = resolve(pkgPath, "utils");
export const themePath = resolve(pkgPath, "theme");

export const proPackage = resolve(proPath, "package.json");
export const compPackage = resolve(componentPath, "package.json");
export const exPackage = resolve(exPath, "package.json");
export const utilPackage = resolve(utilPath, "package.json");

// export const exEsPath = resolve(exOutput, "es");
// export const exLibPath = resolve(exOutput, "lib");

export const exEsPath = resolve(exPath, "es");
export const exLibPath = resolve(exPath, "lib");

export const exTsConfigPath = resolve(proPath, "tsconfig.web.json");
