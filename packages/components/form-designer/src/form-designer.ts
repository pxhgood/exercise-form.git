import { InjectionKey, Ref } from "vue";
import { definePropType } from "@exercise-form/utils";
import {
  DesFormParams,
  DesFormTemplateData,
  DesFormToolbarOptions,
  DesGlobalDvs
} from "@exercise-form/core";

export interface CustomDragEvent extends DragEvent {
  newIndex: number;
}

export const optionsKeys: InjectionKey<Ref<DesFormToolbarOptions>> = Symbol();

export const bannedWidgetKeys: InjectionKey<Ref<Array<string>>> = Symbol();

export const darkKeys: InjectionKey<Ref<boolean>> = Symbol();

export const formDesignerProps = {
  /**
   * @description 构建数据
   */
  formJson: {
    type: definePropType<DesFormParams>(Object),
    default() {
      return {
        widgetList: [],
        formConfig: {}
      };
    }
  },
  /**
   * @description 构造器显示配置项
   */
  options: {
    type: definePropType<DesFormToolbarOptions>(Object),
    default() {
      return {};
    }
  },
  /**
   * @description 组件禁用列表
   */
  bannedWidgets: {
    type: definePropType<Array<string>>(Array),
    default() {
      return [];
    }
  },
  /**
   * @description 模板列表
   */
  templateList: {
    type: definePropType<Array<DesFormTemplateData>>(Array),
    default() {
      return [];
    }
  },
  /**
   *
   * @description 全局变量
   */
  globalDvs: {
    type: definePropType<DesGlobalDvs>(Object),
    default() {
      return {
        testBaseUrl: ""
      };
    }
  },
  /**
   * @description  主题模式
   * vue-codemirror需要dark为true
   */
  dark: Boolean
};
