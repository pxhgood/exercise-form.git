import FormDesigner from "./form-designer.vue";

export type FormDesignerInstance = InstanceType<typeof FormDesigner>;
