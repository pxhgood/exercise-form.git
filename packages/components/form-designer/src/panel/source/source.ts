import { definePropType, getRandomNumber } from "@exercise-form/utils";
import {
  DesFormConfig,
  DesUseWidgetMethods,
  DesFormSource
} from "@exercise-form/core";

export const getSourceForm = () => {
  return {
    sourceId: getRandomNumber(),
    name: "",
    describe: "",
    requestUrl: "",
    requestUrlType: "string",
    method: "POST",
    headers: [],
    params: [],
    data: [],
    configHandlerCode: "return config",
    dataHandlerCode: "return result.data",
    errorHandlerCode: "$message.onMessageError(error)",
    showMultiData: false,
    multiData: []
  } as DesFormSource;
};

export const desSourceProps = {
  designer: {
    type: definePropType<DesUseWidgetMethods>(Object),
    default() {
      return {};
    }
  },
  formConfig: {
    type: definePropType<DesFormConfig>(Object),
    default() {
      return {};
    }
  },
  globalDvs: {
    type: Object,
    default() {
      return {};
    }
  }
};
