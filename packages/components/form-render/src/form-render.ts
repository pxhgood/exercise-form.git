import { InjectionKey, Ref } from "vue";
import {
  DesFormParams,
  DesGlobalDvs,
  DesFormSource
} from "@exercise-form/core";
import { definePropType } from "@exercise-form/utils";

export interface WidgetRefData {
  name: string;
  widgetRef: any;
  [key: string]: any;
}

export const globalFormDataKey: InjectionKey<Ref<{ [key: string]: any }>> =
  Symbol();

export const formRenderRefKey: InjectionKey<Ref<any>> = Symbol();

export type addWidgetRefFn = (data: WidgetRefData) => void;

export const globalDvsKey: InjectionKey<Ref<DesGlobalDvs>> = Symbol();

export type addGlobalDvsFn = (key: string, value: any) => void;

export const dataSourcesKey: InjectionKey<Ref<DesFormSource[]>> = Symbol();

export const formRenderProps = {
  formJson: {
    type: definePropType<DesFormParams>(Object),
    default() {
      return {
        widgetList: [],
        formConfig: {}
      };
    }
  },
  /**
   * @description 表单数据
   */
  formData: {
    type: Object,
    default() {
      return {};
    }
  },
  /**
   *
   * @description 全局变量
   */
  globalDvs: {
    type: definePropType<DesGlobalDvs>(Object),
    default() {
      return {
        testBaseUrl: ""
      };
    }
  },
  /**
   * @description 是否禁用所有表单
   */
  disabled: {
    type: Boolean,
    default: false
  }
};
