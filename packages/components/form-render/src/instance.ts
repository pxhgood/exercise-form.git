import FormRender from "./form-render.vue";

export type FormRenderInstance = InstanceType<typeof FormRender>;
