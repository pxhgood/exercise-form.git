export type ValidatorNameType =
  | "number"
  | "letter"
  | "letterAndNumber"
  | "mobilePhone"
  | "letterStartNumberIncluded"
  | "noChinese"
  | "chinese"
  | "email"
  | "url";

export type ValidatorCallback = (errMsg?: string) => void;

export interface ValidateRules {
  required?: boolean;
  trigger: string[];
  message?: string;
  validator?: (
    rule: ValidateRules,
    value: any,
    callback: ValidatorCallback
  ) => void;
  regExp?: string;
  errorMsg?: string;
  label?: string;
}

export function getRegExp(validatorName: ValidatorNameType) {
  const commonRegExp = {
    number: "/^[-]?\\d+(\\.\\d+)?$/",
    letter: "/^[A-Za-z]+$/",
    letterAndNumber: "/^[A-Za-z0-9]+$/",
    mobilePhone: "/^[1][3-9][0-9]{9}$/",
    letterStartNumberIncluded: "/^[A-Za-z]+[A-Za-z\\d]*$/",
    noChinese: "/^[^\u4e00-\u9fa5]+$/",
    chinese: "/^[\u4e00-\u9fa5]+$/",
    email: "/^([-_A-Za-z0-9.]+)@([_A-Za-z0-9]+\\.)+[A-Za-z0-9]{2,3}$/",
    url: "/^([hH][tT]{2}[pP]:\\/\\/|[hH][tT]{2}[pP][sS]:\\/\\/)(([A-Za-z0-9-~]+)\\.)+([A-Za-z0-9-~\\/])+$/"
  };

  return commonRegExp[validatorName];
}

const validateFn = (
  validatorName: ValidatorNameType,
  rule: ValidateRules,
  value: any,
  callback: ValidatorCallback,
  defaultErrorMsg: string
) => {
  if (!value) {
    callback();
    return;
  }
  const reg = eval(getRegExp(validatorName));

  if (!reg.test(value)) {
    let errTxt = rule.errorMsg || defaultErrorMsg;
    callback(errTxt);
  } else {
    callback();
  }
};

export const fieldValidator = {
  number(rule: ValidateRules, value: any, callback: ValidatorCallback) {
    validateFn(
      "number",
      rule,
      value,
      callback,
      "[" + rule.label + "]包含非数字字符"
    );
  },
  letter(rule: ValidateRules, value: any, callback: ValidatorCallback) {
    validateFn(
      "letter",
      rule,
      value,
      callback,
      "[" + rule.label + "]包含非字母字符"
    );
  },
  letterAndNumber(
    rule: ValidateRules,
    value: any,
    callback: ValidatorCallback
  ) {
    validateFn(
      "letterAndNumber",
      rule,
      value,
      callback,
      "[" + rule.label + "]只能输入字母或数字"
    );
  },
  mobilePhone(rule: ValidateRules, value: any, callback: ValidatorCallback) {
    validateFn(
      "mobilePhone",
      rule,
      value,
      callback,
      "[" + rule.label + "]手机号码格式有误"
    );
  },
  letterStartNumberIncluded(
    rule: ValidateRules,
    value: any,
    callback: ValidatorCallback
  ) {
    validateFn(
      "letterStartNumberIncluded",
      rule,
      value,
      callback,
      "[" + rule.label + "]必须以字母开头，可包含数字"
    );
  },
  noChinese(rule: ValidateRules, value: any, callback: ValidatorCallback) {
    validateFn(
      "noChinese",
      rule,
      value,
      callback,
      "[" + rule.label + "]不可输入中文字符"
    );
  },
  chinese(rule: ValidateRules, value: any, callback: ValidatorCallback) {
    validateFn(
      "chinese",
      rule,
      value,
      callback,
      "[" + rule.label + "]只能输入中文字符"
    );
  },
  email(rule: ValidateRules, value: any, callback: ValidatorCallback) {
    validateFn(
      "email",
      rule,
      value,
      callback,
      "[" + rule.label + "]邮箱格式有误"
    );
  },
  url(rule: ValidateRules, value: any, callback: ValidatorCallback) {
    validateFn(
      "url",
      rule,
      value,
      callback,
      "[" + rule.label + "]邮箱格式有误"
    );
  }
};

export const dataTableValidator = [
  {
    label: "渲染函数",
    options: [
      {
        label: "render",
        value: "render"
      }
    ]
  },
  {
    label: "时间格式",
    options: [
      {
        label: "YYYY-MM-DD",
        value: "YYYY-MM-DD"
      },
      {
        label: "yyyy/MM/dd",
        value: "yyyy/MM/dd"
      },
      {
        label: "yyyy年MM月dd日",
        value: "yyyy年MM月dd日"
      },
      {
        label: "yyyy-MM-dd HH:mm:ss",
        value: "yyyy-MM-dd HH:mm:ss"
      },
      {
        label: "yyyy-MM-dd hh:mm:ss",
        value: "yyyy-MM-dd hh:mm:ss"
      }
    ]
  },
  {
    label: "数字格式",
    options: [
      {
        label: "###,###,###,##0.######",
        value: "###,###,###,##0.######"
      },
      {
        label: "###,###,###,##0.00####",
        value: "###,###,###,##0.00####"
      },
      {
        label: "###,###,###,##0.000000",
        value: "###,###,###,##0.000000"
      },
      {
        label: "###,###,###,##0.000",
        value: "###,###,###,##0.000"
      },
      {
        label: "###,###,###,##0.00",
        value: "###,###,###,##0.00"
      },
      {
        label: "###,###,###,##0",
        value: "###,###,###,##0"
      },
      {
        label: "###,##0.00##%",
        value: "###,##0.00##%"
      }
    ]
  }
];
