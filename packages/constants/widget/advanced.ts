import { DesFormWidget } from "@exercise-form/core";

import { cascaderData } from "./cascader";

export const advanced = [
  {
    name: "级联选择",
    iconName: "cascader",
    type: "cascader",
    formItemFlag: true,
    options: {
      clearable: true,
      customClass: "",
      dataSourceName: "",
      dataSourceSetName: "",
      modelDefaultValue: null,
      disabled: false,
      hidden: false,
      label: "级联选择",
      labelWidth: "",
      labelIcon: "",
      labelIconPosition: "rear",
      labelTooltip: "",
      name: "",
      options: cascaderData,
      onFocus: "",
      onBlur: "",
      onChange: "",
      onValidate: "",
      onCreated: "",
      onMounted: "",
      placeholder: "",
      props: {
        children: "children",
        label: "label",
        value: "value"
      },
      requiredHint: "",
      required: false,
      size: "default",
      showDataSource: false,
      validation: "",
      validationHint: ""
    }
  },
  {
    name: "图片",
    iconName: "image",
    type: "upload-image",
    formItemFlag: true,
    options: {
      action: "#",
      customClass: "",
      data: {},
      drag: false,
      disabled: false,
      fileMaxSize: 5,
      fileTypes: ["jpg", "jpeg", "png"],
      headers: {},
      hidden: false,
      method: "POST",
      multiple: false,
      label: "上传图片",
      labelWidth: "",
      labelIcon: "",
      labelIconPosition: "rear",
      labelTooltip: "",
      limit: 5,
      onBeforeUpload: "",
      onSuccess: "",
      onBeforeRemove: "",
      onRemove: "",
      onError: "",
      onCreated: "",
      onMounted: "",
      onValidate: "",
      requiredHint: "",
      required: false,
      validation: "",
      validationHint: "",
      withCredentials: false
    }
  },
  {
    name: "富文本",
    iconName: "rich-text",
    type: "rich-text",
    formItemFlag: true,
    options: {
      customClass: "",
      hidden: false,
      maxLength: null,
      label: "富文本内容",
      labelWidth: "",
      labelIcon: "",
      labelIconPosition: "rear",
      labelTooltip: "",
      onValidate: "",
      onCreated: "",
      onMounted: "",
      placeholder: "请输入...",
      readOnly: false,
      requiredHint: "",
      required: false,
      validation: "",
      validationHint: "",
      withCredentials: false
    }
  },
  {
    name: "文件",
    iconName: "file",
    type: "upload-file",
    formItemFlag: true,
    options: {
      action: "#",
      customClass: "",
      data: {},
      drag: false,
      disabled: false,
      fileMaxSize: 5,
      fileTypes: ["doc", "docx", "xlsx", "xls"],
      headers: {},
      hidden: false,
      method: "POST",
      multiple: false,
      label: "上传文件",
      labelWidth: "",
      labelIcon: "",
      labelIconPosition: "rear",
      labelTooltip: "",
      limit: 5,
      onBeforeUpload: "",
      onSuccess: "",
      onBeforeRemove: "",
      onRemove: "",
      onError: "",
      onValidate: "",
      onCreated: "",
      onMounted: "",
      requiredHint: "",
      required: false,
      validation: "",
      validationHint: "",
      withCredentials: false
    }
  },
  {
    name: "插槽",
    iconName: "slot",
    type: "slot",
    options: {
      customClass: "",
      name: "",
      label: "slot"
    }
  }
] as DesFormWidget[];
