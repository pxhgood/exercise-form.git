export const cascaderData = [
  {
    label: "new option",
    value: 1,
    children: [
      {
        label: "new option",
        value: "1-1",
        children: []
      }
    ]
  },
  {
    label: "new option",
    value: 2,
    children: []
  },
  {
    label: "new option",
    value: 3,
    children: []
  }
];
