//日期类型
export const dateOptions = [
  {
    value: "date",
    label: "date"
  },
  {
    value: "year",
    label: "year"
  },
  {
    value: "month",
    label: "month"
  },
  {
    value: "dates",
    label: "dates"
  },
  {
    value: "datetime",
    label: "datetime"
  },
  {
    value: "week",
    label: "week"
  }
];

// 日期范围
export const dateRangeOptions = [
  {
    value: "datetimerange",
    label: "datetimerange"
  },
  {
    value: "daterange",
    label: "daterange"
  },
  {
    value: "monthrange",
    label: "monthrange"
  }
];
// 日期格式
export const dateFormatOptions = [
  {
    value: "YYYY-MM-DD",
    label: "YYYY-MM-DD"
  },
  {
    value: "YYYY/MM/DD",
    label: "YYYY/MM/DD"
  },
  {
    value: "YYYY年MM月DD日",
    label: "YYYY年MM月DD日"
  },
  {
    value: "YYYY-MM-DD HH:mm:ss",
    label: "YYYY-MM-DD HH:mm:ss"
  },
  {
    value: "YYYY-MM-DD hh:mm:ss",
    label: "YYYY-MM-DD hh:mm:ss"
  }
];
// 时间格式
export const timeFormatOptions = [
  {
    value: "HH:mm:ss",
    label: "HH:mm:ss"
  },
  {
    value: "hh:mm:ss",
    label: "hh:mm:ss"
  },
  {
    value: "HH时mm分ss秒",
    label: "HH时mm分ss秒"
  }
];

export const tagOptions = [
  {
    value: "primary",
    label: "primary"
  },
  {
    value: "success",
    label: "success"
  },
  {
    value: "warning",
    label: "warning"
  },
  {
    value: "info",
    label: "info"
  },
  {
    value: "danger",
    label: "danger"
  }
];

export const directionOptions = [
  {
    value: "none",
    label: "none"
  },
  {
    value: "solid",
    label: "solid"
  },
  {
    value: "hidden",
    label: "hidden"
  },
  {
    value: "dashed",
    label: "dashed"
  },
  {
    value: "dotted",
    label: "dotted"
  }
];

export const descriptionOptions = [
  {
    value: "success",
    label: "success"
  },
  {
    value: "warning",
    label: "warning"
  },
  {
    value: "info",
    label: "info"
  },
  {
    value: "error",
    label: "error"
  }
];
// 表单校验
export const validationOptions = [
  {
    type: "number",
    label: "数字",
    rule: ""
  },
  {
    type: "letter",
    label: "字母",
    rule: ""
  },
  {
    type: "letterAndNumber",
    label: "字母数字",
    rule: ""
  },

  {
    type: "mobilePhone",
    label: "手机号码",
    rule: ""
  },
  {
    type: "letterStartNumberIncluded",
    label: "字母开头，可包含数字",
    rule: ""
  },

  {
    type: "noChinese",
    label: "非中文字符",
    rule: ""
  },
  {
    type: "chinese",
    label: "仅中文字符",
    rule: ""
  },
  {
    type: "email",
    label: "邮箱",
    rule: ""
  },
  {
    type: "url",
    label: "网址",
    rule: ""
  }
];

export const buttonTypeList = [
  {
    value: "default",
    label: ""
  },
  {
    value: "primary",
    label: "primary"
  },
  {
    value: "success",
    label: "success"
  },
  {
    value: "warning",
    label: "warning"
  },
  {
    value: "info",
    label: "info"
  },
  {
    value: "danger",
    label: "danger"
  }
];

export const sizeTypeList = [
  {
    value: "large",
    label: "large"
  },
  {
    value: "default",
    label: "default"
  },
  {
    value: "small",
    label: "small"
  }
];

export const fixedTypeList = [
  {
    value: "left",
    label: "left"
  },
  {
    value: "false",
    label: ""
  },
  {
    value: "right",
    label: "right"
  }
];

export const alignTypeList = [
  {
    value: "left",
    label: "left"
  },
  {
    value: "center",
    label: "center"
  },
  {
    value: "right",
    label: "right"
  }
];

export const fileTypeList = [
  {
    value: "doc",
    label: "doc"
  },
  {
    value: "docx",
    label: "docx"
  },
  {
    value: "xlsx",
    label: "xlsx"
  },
  {
    value: "xls",
    label: "xls"
  }
];

export const imageTypeList = [
  {
    value: "png",
    label: "png"
  },
  {
    value: "jpeg",
    label: "jpeg"
  },
  {
    value: "jpg",
    label: "jpg"
  }
];
