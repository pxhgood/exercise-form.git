// 模式
export const PATTERN_TYPE = ["pc", "pad", "h5"] as const;

// 禁止拖入容器
export const DRAG_DISABLE_LIST = ["popup-box", "side-drawer"];

// 可导出属性表单
export const MODEL_LIST = [
  "input",
  "input-number",
  "radio",
  "checkbox",
  "select",
  "date-picker",
  "time-picker",
  "switch",
  "cascader",
  "rate",
  "slider",
  "color-picker",
  "sub-form",
  "grid-sub-form",
  "object-group"
];

// 请求方式
export const METHOD_LIST = ["GET", "POST", "PUT", "DELETE"];

// 含有可导出数据的属性
export const HAVE_DATA_KEYS = [
  "data",
  "headers",
  "tableData",
  "treeData",
  "optionsItem",
  "options"
];

// 外部组件
export const OUTER_WIDGET = ["popup-box", "side-drawer"];

export const DATA_TYPE = ["string", "number", "boolean"];
