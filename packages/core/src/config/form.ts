import { DesFormConfig } from "../interface";

export const getFormConfig = (): DesFormConfig => {
  return {
    size: "default",
    labelPosition: "left",
    align: "left",
    labelWidth: 100,
    dataSources: [],
    modelName: "formData",
    formName: "formRef",
    rulesName: "rules",
    patternType: "pc",
    isPageType: "page",
    hideRequiredAsterisk: false,
    cssCode: "",
    customClass: [],
    functions: "",
    onFormCreated: "",
    onFormMounted: "",
    onFormDataChange: ""
  };
};
