/**
 * @description 构建基础组件模板
 */

import { getElAttr } from "./property";
import {
  DesFormWidget,
  DesFormWidgetMethods,
  DesFormWidgetParams
} from "../interface";
import { getEventAttr } from "./event-method";

const basicsTemplates = {
  input: (params) => {
    let {
      modelValue,
      disabled,
      maxLength,
      minLength,
      size,
      readonly,
      placeholder,
      type,
      rows,
      showWordLimit,
      prefixIcon,
      suffixIcon,
      clearable
    } = getElAttr(params);

    let { widget } = params;
    let {
      onInput,
      onFocus,
      onChange,
      onBlur,
      onClear,
      onFrontAppendButtonClick,
      onRearAppendButtonClick
    } = getEventAttr(widget);

    let {
      frontAppendType,
      rearAppendType,
      appendRearButtonDisabled,
      appendFrontButtonDisabled,
      rearButtonIcon,
      frontButtonIcon,
      appendButton,
      rearTextForAppend,
      frontTextForAppend
    } = widget.options;

    let frontDisabledAttr = appendFrontButtonDisabled ? `disabled` : "";
    let rearDisabledAttr = appendRearButtonDisabled ? `disabled` : "";

    let frontIconAttr = frontButtonIcon ? `icon="${frontButtonIcon}"` : "";
    let rearIconAttr = rearButtonIcon ? `icon="${rearButtonIcon}"` : "";

    let prependTemplate =
      appendButton?.includes("prepend") && (frontAppendType || rearAppendType)
        ? `<template #prepend>
        <el-button ${frontDisabledAttr} ${frontIconAttr} ${onFrontAppendButtonClick}>${frontTextForAppend}</el-button>
        </template>`
        : "";
    let appendTemplate =
      appendButton?.includes("append") && (frontAppendType || rearAppendType)
        ? `<template #append>
            <el-button ${rearDisabledAttr} ${rearIconAttr} ${onRearAppendButtonClick}>${rearTextForAppend}</el-button>
          </template>`
        : "";

    return `<el-input ${modelValue} ${type} ${disabled} ${maxLength} 
      ${minLength} ${size} ${rows} ${prefixIcon} ${suffixIcon} ${readonly} ${showWordLimit} ${placeholder} 
      ${clearable} ${onInput} ${onFocus} ${onBlur} ${onChange} ${onClear}>${prependTemplate}${appendTemplate}</el-input>`;
  },

  "input-number": (params) => {
    let {
      modelValue,
      disabled,
      size,
      controls,
      contentPosition,
      max,
      min,
      step,
      precision,
      stepStrictly,
      readonly,
      placeholder
    } = getElAttr(params);

    let { widget } = params;
    let { onFocus, onChange, onBlur } = getEventAttr(widget);

    return `<el-input-number ${modelValue} ${disabled} ${size} ${controls} ${contentPosition} ${precision}
    ${max} ${min} ${step} ${stepStrictly} ${readonly} ${placeholder} ${onFocus} ${onBlur} ${onChange} />`;
  },

  radio: (params) => {
    let { widget } = params;
    let { modelValue, disabled, size, border, displayStyle } =
      getElAttr(params);
    let childTemplate = buildRadioChildren(widget);
    let { onChange } = getEventAttr(widget);

    return `<el-radio-group ${displayStyle} ${modelValue} ${size} ${disabled} 
    ${border} ${onChange}>${childTemplate}</el-radio-group>`;
  },

  checkbox: (params) => {
    let { widget } = params;
    let { modelValue, disabled, size, border } = getElAttr(params);
    let childTemplate = buildCheckboxChildren(widget);
    let { onChange } = getEventAttr(widget);

    return `<el-checkbox-group ${modelValue} ${size} ${disabled} 
    ${border} ${onChange}>${childTemplate}</el-checkbox-group>`;
  },

  select: (params) => {
    let { widget } = params;
    let {
      modelValue,
      disabled,
      size,
      multiple,
      multipleLimit,
      collapseTags,
      collapseTagsTooltip,
      maxCollapseTags,
      clearable
    } = getElAttr(params);
    let childTemplate = buildSelectChildren(widget);
    let { onFocus, onChange, onBlur, onClear, onRemoveTag } =
      getEventAttr(widget);

    return `<el-select ${modelValue} ${size} ${disabled} ${multiple} ${multipleLimit} 
    ${collapseTags} ${collapseTagsTooltip} ${maxCollapseTags} ${clearable} ${onFocus} ${onBlur} ${onChange} 
    ${onRemoveTag} ${onClear}>${childTemplate}</el-select>`;
  },

  "date-picker": (params) => {
    let {
      modelValue,
      type,
      disabled,
      size,
      editable,
      format,
      clearable,
      placeholder,
      readonly,
      startPlaceholder,
      endPlaceholder
    } = getElAttr(params);
    let { widget } = params;
    let { onFocus, onChange, onBlur } = getEventAttr(widget);

    return `<el-date-picker ${modelValue} ${type} ${size} ${disabled} ${editable} 
    ${format} ${clearable} ${placeholder} ${readonly} ${startPlaceholder} 
    ${endPlaceholder} ${onFocus} ${onBlur} ${onChange}/>`;
  },

  "time-picker": (params) => {
    let {
      modelValue,
      type,
      disabled,
      size,
      editable,
      format,
      clearable,
      placeholder,
      readonly,
      startPlaceholder,
      endPlaceholder
    } = getElAttr(params);
    let { widget } = params;
    let { onFocus, onChange, onBlur } = getEventAttr(widget);

    return `<el-time-picker ${modelValue} ${type} ${size} ${disabled} ${editable} ${format} 
      ${clearable} ${placeholder} ${readonly} ${startPlaceholder} 
      ${endPlaceholder} ${onFocus} ${onBlur} ${onChange}/>`;
  },

  switch: (params) => {
    let { widget } = params;
    let {
      modelValue,
      disabled,
      size,
      width,
      activeIcon,
      inactiveIcon,
      activeActionIcon,
      inactiveActionIcon,
      activeText,
      inactiveText
    } = getElAttr(params);
    let {
      activeValue,
      inactiveValue,
      activeValueType,
      inactiveValueType,
      switchOnColor,
      switchOffColor,
      switchBorderColor
    } = widget.options;
    let { onChange } = getEventAttr(widget);

    let onColor = switchOnColor ? `--el-switch-on-color:${switchOnColor};` : "";
    let offColor = switchOffColor
      ? `--el-switch-off-color:${switchOffColor};`
      : "";
    let borderColor = switchBorderColor
      ? `--el-switch-border-color:${switchBorderColor};`
      : "";
    let style =
      offColor || offColor || borderColor
        ? `style="${onColor} ${offColor} ${borderColor}"`
        : "";

    let activeValueAttr = "";
    let inactiveValueAttr = "";

    if (activeValueType === "string") {
      activeValueAttr = `active-value="${activeValue}"`;
    }
    if (["number", "boolean"].includes(activeValueType)) {
      activeValueAttr = `:active-value="${activeValue}"`;
    }

    if (inactiveValueType === "string") {
      inactiveValueAttr = `inactive-value="${inactiveValue}"`;
    }
    if (["number", "boolean"].includes(inactiveValueType)) {
      inactiveValueAttr = `:inactive-value="${inactiveValue}"`;
    }

    return `<el-switch ${style} ${width} ${modelValue} ${activeValueAttr} ${inactiveValueAttr} 
    ${disabled} ${size} ${activeIcon} ${inactiveIcon} ${activeActionIcon} ${inactiveActionIcon} 
    ${activeText} ${inactiveText} ${onChange}/>`;
  },

  cascader: (params) => {
    let { modelValue, disabled, size, clearable, placeholder } =
      getElAttr(params);
    let { widget } = params;
    let { onFocus, onChange, onBlur } = getEventAttr(widget);

    return `<el-autocomplete ${modelValue} ${disabled} ${size} ${clearable} 
    ${placeholder} ${onFocus} ${onBlur} ${onChange}/>`;
  },

  rate: (params) => {
    let {
      modelValue,
      allowHalf,
      max,
      disabled,
      size,
      lowThreshold,
      highThreshold,
      showText,
      showScore,
      clearable
    } = getElAttr(params);
    let { widget } = params;
    let { onChange } = getEventAttr(widget);

    return `<el-rate ${modelValue} ${max} ${lowThreshold} ${highThreshold} ${disabled} 
    ${showText} ${showScore} ${size} ${clearable} ${allowHalf} ${onChange}/>`;
  },

  button: (params) => {
    let { widget } = params;
    let { disabled, size, type, plain, link, round, circle, text } =
      getElAttr(params);
    let { onClick } = getEventAttr(widget);
    let { icon, label } = widget.options;
    let iconAttr = icon ? `icon="${icon}"` : "";

    return `<el-button ${type} ${disabled} ${size} ${plain} ${link} ${round}
    ${circle} ${text} ${iconAttr} ${onClick}>${label}</el-button>`;
  },

  divider: (params) => {
    let { borderStyle, contentPosition, direction } = getElAttr(params);
    return `<el-divider ${borderStyle} ${contentPosition} ${direction} />`;
  },

  slider: (params) => {
    let { widget } = params;
    let { modelValue, disabled, step, showStops, max, min, marks } =
      getElAttr(params);
    let { onInput, onChange } = getEventAttr(widget);

    return `<el-slider ${modelValue} ${min} ${max} ${marks} ${disabled} ${showStops} ${step} ${onInput} ${onChange}/>`;
  },

  "color-picker": (params) => {
    let { modelValue, disabled, size } = getElAttr(params);
    let { widget } = params;
    let { onFocus, onChange, onBlur } = getEventAttr(widget);

    return `<el-color-picker ${modelValue} ${disabled} ${size} ${onFocus} ${onBlur} ${onChange}/>`;
  },

  alert: (params) => {
    let { closable, showIcon, title, type } = getElAttr(params);
    return `<el-alert ${type} ${closable} ${showIcon} ${title}  />`;
  },

  text: (params) => {
    let { widget } = params;
    let { fontSize, size, tag, truncated, customClass } = getElAttr(params);
    return `<el-text ${fontSize} ${customClass} ${size} ${tag} ${truncated} >${widget.options.content}</el-text>`;
  },

  slot: (params) => {
    let { widget } = params;
    let name = widget.options.label ? `name="${widget.options.label}"` : "";
    return `<slot ${name}></slot>`;
  },

  "upload-image": (params) => {
    let {
      fileList,
      action,
      accept,
      headers,
      data,
      method,
      limit,
      multiple,
      disabled,
      withCredentials
    } = getElAttr(params);
    let { onBeforeUpload, onBeforeRemove, onSuccess, onRemove, onError } =
      getEventAttr(params.widget);

    return `<el-upload ${fileList} ${action} ${accept} ${headers} ${data} ${multiple} ${limit} ${disabled}
    ${withCredentials} ${method} ${onBeforeUpload} ${onSuccess} ${onBeforeRemove} ${onRemove} ${onError}
    list-type="picture-card"> <el-icon><Plus /></el-icon></el-upload>`;
  },

  "upload-file": (params) => {
    let {
      fileList,
      action,
      accept,
      headers,
      data,
      method,
      limit,
      multiple,
      disabled,
      withCredentials
    } = getElAttr(params);
    let { onBeforeUpload, onBeforeRemove, onSuccess, onRemove, onError } =
      getEventAttr(params.widget);

    return `<el-upload ${fileList} ${action} ${accept} ${headers} ${data} ${method} ${multiple} ${limit} ${disabled}
    ${withCredentials} ${onBeforeUpload} ${onSuccess} ${onBeforeRemove} ${onRemove} ${onError}></el-upload>`;
  },

  "rich-text": (params) => {
    return ``;
  }
} as DesFormWidgetMethods;

function buildRadioChildren(widget: DesFormWidget) {
  let { buttonMode, border, optionsLabel, optionsValue, displayStyle } =
    widget.options;
  let borderAttr = border && !buttonMode ? `:border` : "";
  let tag = buttonMode ? "el-radio-button" : "el-radio";
  let displayStyleAttr = displayStyle ? `style="display:${displayStyle};"` : "";
  return `<${tag} ${displayStyleAttr} v-for="item in ${widget.options.name}optionsItem" 
  :key="item.${optionsValue}"  :value="item.${optionsValue}" :label="item.${optionsLabel}" ${borderAttr}></${tag}>`;
}

function buildCheckboxChildren(widget: DesFormWidget) {
  let { buttonMode, border, optionsLabel, optionsValue, displayStyle } =
    widget.options;
  let displayStyleAttr = displayStyle ? `style="display:${displayStyle};"` : "";
  let borderAttr = border && !buttonMode ? `:border` : "";
  let tag = buttonMode ? "el-checkbox-button" : "el-checkbox";
  return `<${tag} ${displayStyleAttr} v-for="item in ${widget.options.name}optionsItem" 
  :key="item.${optionsValue}" :value="item.${optionsValue}" :label="item.${optionsLabel}" ${borderAttr}></${tag}>`;
}

function buildSelectChildren(widget: DesFormWidget) {
  let { optionsLabel, optionsValue } = widget.options;
  return `<el-option v-for="item in ${widget.options.name}optionsItem" 
  :key="item.${optionsValue}" :label="item.${optionsLabel}" :value="item.${optionsValue}"/>`;
}

export function buildBasicsTemplate(params: DesFormWidgetParams) {
  let { widget } = params;
  return basicsTemplates[widget.type] && basicsTemplates[widget.type](params)
    ? basicsTemplates[widget.type](params)
    : "";
}
