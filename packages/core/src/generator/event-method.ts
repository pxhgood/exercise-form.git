/**
 * @description 组件事件构建
 * 事件函数名命名规则：组件名（name）与事件名（例：Click）拼接
 */
import { hasOwn, isArray } from "@exercise-form/utils";

import { DesFormWidget } from "../interface";

interface EventOptions {
  defaultFnBody: string;
  params: string;
  fnName: string;
}

export const globalEventMethods = {
  onCreated: {
    defaultFnBody: "",
    params: "",
    fnName: "onCreated"
  },

  onMounted: {
    defaultFnBody: "",
    params: "",
    fnName: "onMounted"
  },

  onClick: {
    defaultFnBody: "",
    params: "e",
    fnName: "Click"
  },

  onInput: {
    defaultFnBody: "",
    params: "value",
    fnName: "Input"
  },

  onChange: {
    defaultFnBody: "",
    params: "value",
    fnName: "Change"
  },

  onFocus: {
    defaultFnBody: "",
    params: "e",
    fnName: "Focus"
  },

  onBlur: {
    defaultFnBody: "",
    params: "e",
    fnName: "Blur"
  },

  onValidate: {
    defaultFnBody: "",
    params: "rules,value,callback",
    fnName: "Validate"
  },

  onClear: {
    defaultFnBody: "",
    params: "e",
    fnName: "Clear"
  },

  onRemoveTag: {
    defaultFnBody: "",
    params: "tagValue",
    fnName: "RemoveTag"
  },

  onFrontAppendButtonClick: {
    defaultFnBody: "",
    params: "e",
    fnName: "FrontAppendButtonClick"
  },

  onRearAppendButtonClick: {
    defaultFnBody: "",
    params: "e",
    fnName: "RearAppendButtonClick"
  },

  //tabs
  onTabClick: {
    defaultFnBody: "",
    params: "pane,e",
    fnName: "TabClick"
  },

  // tree
  onNodeClick: {
    defaultFnBody: "",
    params: "data, node, el",
    fnName: "NodeClick"
  },

  onNodeCheck: {
    defaultFnBody: "",
    params: "data",
    fnName: "NodeCheck"
  },

  onCheckChange: {
    defaultFnBody: "",
    params: "node",
    fnName: "CheckChange"
  },

  // dialog or drawer
  onCancelButtonClick: {
    defaultFnBody: "",
    params: "e",
    fnName: "CancelButtonClick"
  },

  onConfirmButtonClick: {
    defaultFnBody: "",
    params: "e",
    fnName: "ConfirmButtonClick"
  },

  //dialog
  onDialogClose: {
    defaultFnBody: "",
    params: "e",
    fnName: "DialogClose"
  },

  onDialogOpened: {
    defaultFnBody: "",
    params: "e",
    fnName: "DialogOpened"
  },

  // drawer
  onDrawerClose: {
    defaultFnBody: "",
    params: "e",
    fnName: "DrawerClose"
  },

  onDrawerOpened: {
    defaultFnBody: "",
    params: "e",
    fnName: "DrawerOpened"
  },

  // table
  onSelect: {
    defaultFnBody: "",
    params: "selection, row",
    fnName: "Select"
  },

  onSelectAll: {
    defaultFnBody: "",
    params: "selection",
    fnName: "SelectAll"
  },

  onSelectionChange: {
    defaultFnBody: "",
    params: "newSelection",
    fnName: "SelectionChange"
  },

  onCellClick: {
    defaultFnBody: "",
    params: "row, column, cell, event",
    fnName: "CellClick"
  },

  onRowClick: {
    defaultFnBody: "",
    params: "row, column, event",
    fnName: "CellClick"
  },

  onHeaderClick: {
    defaultFnBody: "",
    params: "e",
    fnName: "HeaderClick"
  },

  onSortChange: {
    defaultFnBody: "",
    params: "column, prop, order",
    fnName: "SortChange"
  },

  onFilterChange: {
    defaultFnBody: "",
    params: "newFilters",
    fnName: "FilterChange"
  },

  onExpandChange: {
    defaultFnBody: "",
    params: "row, expandedRows",
    fnName: "ExpandChange"
  },

  onPageSizeChange: {
    defaultFnBody: "",
    params: "value",
    fnName: "PageSizeChange"
  },

  onCurrentPageChange: {
    defaultFnBody: "",
    params: "value",
    fnName: "CurrentPageChange"
  },

  onTableColumnClick: {
    defaultFnBody: "",
    params: "row,index",
    fnName: "TableColumnClick"
  },

  // sub-form
  onSubFormRowAdd: {
    defaultFnBody: "",
    params: "parentData,widgetName",
    fnName: "SubFormRowAdd"
  },

  onSubFormRowInsert: {
    defaultFnBody: "",
    params: "parentData,widgetName,index",
    fnName: "SubFormRowInsert"
  },

  onSubFormRowDelete: {
    defaultFnBody: "",
    params: "parentData,widgetName,index",
    fnName: "SubFormRowDelete"
  },

  // upload file
  onBeforeUpload: {
    defaultFnBody: "",
    params: "file",
    fnName: "BeforeUpload"
  },

  onSuccess: {
    defaultFnBody: "",
    params: "result, file, fileList",
    fnName: "Success"
  },

  onBeforeRemove: {
    defaultFnBody: "",
    params: "file, fileList",
    fnName: "BeforeRemove"
  },

  onRemove: {
    defaultFnBody: "",
    params: "file, fileList",
    fnName: "Remove"
  },

  onError: {
    defaultFnBody: "",
    params: "error,file, fileList",
    fnName: "Error"
  }
} as { [key: string]: EventOptions };

export function getEventAttr(widget: DesFormWidget) {
  let wop = widget.options;
  return {
    onClick: wop.onClick ? `@click="${wop.name}Click"` : "",
    onInput: wop.onInput ? `@input="${wop.name}Input"` : "",
    onChange: wop.onChange ? `@change="${wop.name}Change"` : "",
    onFocus: wop.onFocus ? `@focus="${wop.name}Focus"` : "",
    onBlur: wop.onBlur ? `@blur="${wop.name}Blur"` : "",
    onClear: wop.onClear ? `@clear="${wop.name}Clear"` : "",
    onValidate: wop.onValidate ? `:validate="${wop.name}Validate"` : "",

    onFrontAppendButtonClick: wop.onFrontAppendButtonClick
      ? `@click="${wop.name}FrontAppendButtonClick"`
      : "",
    onRearAppendButtonClick: wop.onRearAppendButtonClick
      ? `@click="${wop.name}RearAppendButtonClick"`
      : "",

    onRemoveTag: wop.onRemoveTag ? `@remove-tag="${wop.name}RemoveTag"` : "",

    onSelect: wop.onSelect ? `@select="${wop.name}Select "` : "",
    onSelectAll: wop.onSelectAll ? `@select-all="${wop.name}SelectAll"` : "",
    onSelectionChange: wop.onSelectionChange
      ? `@selection-change="${wop.name}SelectionChange"`
      : "",
    onCellClick: wop.onCellClick ? `@cell-click="${wop.name}CellClick"` : "",
    onRowClick: wop.onRowClick ? `@row-click="${wop.name}RowClick"` : "",
    onHeaderClick: wop.onHeaderClick
      ? `@header-click="${wop.name}HeaderClick"`
      : "",
    onSortChange: wop.onSortChange
      ? `@sort-change="${wop.name}SortChange"`
      : "",
    onFilterChange: wop.onFilterChange
      ? `@filter-change="${wop.name}FilterChange"`
      : "",
    onExpandChange: wop.onExpandChange
      ? `@expand-change="${wop.name}ExpandChange"`
      : "",
    onPageSizeChange: wop.onPageSizeChange
      ? `@size-change="${wop.name}PageSizeChange"`
      : "",
    onCurrentPageChange: wop.onCurrentPageChange
      ? `@current-change="${wop.name}CurrentPageChange"`
      : "",
    onTableColumnClick: wop.onTableColumnClick
      ? `@click="${wop.name}TableColumnClick"`
      : "",

    onNodeClick: wop.onNodeClick ? `@node-click="${wop.name}NodeClick"` : "",
    onNodeCheck: wop.onNodeCheck ? `@check="${wop.name}NodeCheck"` : "",
    onCheckChange: wop.onCheckChange
      ? `@check-change="${wop.name}CheckChange"`
      : "",

    onBeforeUpload: wop.onBeforeUpload ? `:before-upload="${wop.name}"` : "",
    onSuccess: wop.onSuccess ? `:on-success="${wop.name}"` : "",
    onBeforeRemove: wop.onBeforeUpload ? `:before-remove="${wop.name}"` : "",
    onRemove: wop.onRemove ? `:on-remove="${wop.name}"` : "",
    onError: wop.onError ? `:on-error="${wop.name}"` : ""
  };
}

function getEventMethodsString(
  eventName: string,
  prefixName: string,
  fnBody: string
) {
  let { params, fnName } = globalEventMethods[eventName];
  return `const ${prefixName}${fnName}=(${params})=>{${fnBody}}`;
}

export function getWidgetEventList(widget: DesFormWidget) {
  let eventFnList: string[] = [];
  let { type, options } = widget;
  let { name, operationButtons, buttonGroups } = options;

  function addEventFn(object: { [key: string]: any }, prefix: string) {
    for (const key in object) {
      if (object[key] && hasOwn(globalEventMethods, key)) {
        eventFnList.push(getEventMethodsString(key, prefix, object[key]));
      }
    }
  }

  addEventFn(options, name);
  if (type === "data-table" && isArray(operationButtons)) {
    operationButtons.forEach((button) => {
      addEventFn(button, button.name);
    });
  }
  if (type === "button-group" && isArray(buttonGroups)) {
    buttonGroups.forEach((button) => {
      addEventFn(button, button.name);
    });
  }
  return eventFnList;
}
