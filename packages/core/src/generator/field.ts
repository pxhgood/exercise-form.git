/**
 * @description 构建el-form-item模板
 */
import { DesFormWidgetParams } from "../interface";
import { buildBasicsTemplate } from "./basics";
import { getEventAttr } from "./event-method";
import { getElAttr } from "./property";

export function buildFieldWidget(params: DesFormWidgetParams) {
  let { widget } = params;
  let { customClass, labelWidth, align, required, chainedList } =
    getElAttr(params);
  let template = buildBasicsTemplate(params);
  let { onValidate } = getEventAttr(widget);

  if (!widget.formItemFlag) {
    return template;
  } else {
    let { labelIcon, labelTooltip, labelIconPosition, label } = widget.options;

    let propList = chainedList?.map((c, index) => {
      if (c == "0") {
        return (c = `\${${chainedList[index - 1]}Index}`);
      }
      return c;
    });

    let propAttr = `:prop="\`${propList?.join(".")}\`"`;
    let labelTooltipAttr = labelTooltip ? `title= "${labelTooltip}"` : "";
    let labelIconAttr = `<el-icon :size="14"><${labelIcon} /></el-icon>`;

    let labelTemplate = labelIcon
      ? `<template #label><span>
      ${labelIconPosition === "rear" ? "" : `${labelIconAttr}`}
      ${label}
      ${labelIconPosition === "rear" ? `${labelIconAttr}` : ""}
      </span></template>`
      : "";

    let labelAttr = label && !labelIcon ? `label="${label}"` : "";

    return `<el-form-item ${customClass} ${labelAttr} ${propAttr} ${required} 
    ${labelWidth} ${align} ${labelTooltipAttr} ${onValidate}>${labelTemplate}${template}</el-form-item>`;
  }
}
