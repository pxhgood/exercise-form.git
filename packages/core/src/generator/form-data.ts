import {
  hasOwn,
  isArray,
  isNumber,
  isObject,
  objectToTemplateString
} from "@exercise-form/utils";

import { MODEL_LIST } from "../config";
import { DesFormParams, DesFormWidget } from "../interface";
import { traverseFieldWidget } from "./vue3Js";

enum AttributeType {
  Array = "Array",
  Object = "Object"
}

interface ContainerOptionType {
  name: string;
  type: "Array" | "Object";
  show: boolean;
}

type NameMapValue = { [key: string]: any };

interface ContainerMapKeys {
  id: string;
  parenId: string;
}

interface ContainerMapValue {
  type: string;
  name: string;
  show: boolean;
}

type ContainerMap = Map<ContainerMapKeys, ContainerMapValue>;

type ContainerWidgetNameMap = Map<string, NameMapValue>;

// 是否需要开启隐藏新行，目的为获取完成表单数据
let openShowBlankRow = true;
let FORM_OBJECT_KEY = "FORM_OBJECT_KEY";

function _isObject(obj: unknown) {
  return Object.prototype.toString.call(obj) === "[object Object]";
}

function getContainerWidget(containerList: ContainerMap) {
  return function (widget: DesFormWidget, parent?: DesFormWidget) {
    let { category, id, type } = widget;
    if (category === "container") {
      let { name, showBlankRow } = widget.options;
      let parenId = parent?.id || null;
      let keys = { parenId, id };
      containerList.set(keys, { type, name, show: showBlankRow });
    }
  };
}

/**
 *
 * @param list 外层对象列表
 * @param containerList 所有容器列表
 * @param parenId 上层容器id
 */
function getOuterContainerObj(
  list: ContainerOptionType[],
  containerList: ContainerMap,
  parenId: string
) {
  for (let [key, value] of containerList.entries()) {
    if (key.id == parenId) {
      let { type, name, show } = value;
      if (type === "grid-sub-form" || type === "sub-form") {
        list.push({
          name: name,
          type: AttributeType.Array,
          show: openShowBlankRow ? show : true
        });
      }
      if (type === "object-group") {
        list.push({ name, type: AttributeType.Object, show: true });
      }
      if (key.parenId) {
        getOuterContainerObj(list, containerList, key.parenId);
      }
    }
  }
}

/**
 * 构建formData Map数据
 * @param containerList 容器关系列表
 * @param containerKeysMap Map数据
 * @returns
 */
function buildDefaultValueListFn(
  containerList: ContainerMap,
  containerKeysMap: ContainerWidgetNameMap
) {
  return function (widget: DesFormWidget, parent?: DesFormWidget) {
    if (MODEL_LIST.includes(widget.type)) {
      let { modelDefaultValue, name, showBlankRow } = widget.options;
      let value;
      if (
        isArray(modelDefaultValue) ||
        isObject(modelDefaultValue) ||
        isNumber(modelDefaultValue)
      ) {
        value = modelDefaultValue;
      } else {
        if (["sub-form", "grid-sub-form"].includes(widget.type)) {
          value = showBlankRow ? [{}] : [];
        } else if (widget.type === "object-group") {
          value = {};
        } else {
          value = modelDefaultValue ? `${modelDefaultValue}` : null;
        }
      }
      let list = [] as ContainerOptionType[];
      if (parent) {
        getOuterContainerObj(list, containerList, parent.id);
      }
      if (list.length > 0) {
        let outer = list[list.length - 1];
        let data = arrayToNestedObject(list, outer.show);
        if (!containerKeysMap.has(outer.name)) {
          containerKeysMap.set(outer.name, data);
        }
        if (outer.show) {
          let thisData = containerKeysMap.get(outer.name) || {};
          const mergedObj = mergeObjects(thisData, data, outer.name);
          containerKeysMap.set(outer.name, mergedObj);
          let newData = containerKeysMap.get(outer.name) || {};
          findAndAddProperty(newData, list.reverse(), name, value);
        }
      } else {
        if (
          ["sub-form", "grid-sub-form", "object-group"].includes(widget.type)
        ) {
          if (!containerKeysMap.has(name)) {
            containerKeysMap.set(name, { [name]: value });
          }
        } else {
          if (!containerKeysMap.has(FORM_OBJECT_KEY)) {
            containerKeysMap.set(FORM_OBJECT_KEY, []);
          }
          let thisData = containerKeysMap.get(FORM_OBJECT_KEY) || {};
          thisData[name] = value;
          containerKeysMap.set(FORM_OBJECT_KEY, thisData);
        }
      }
    }
  };
}

/**
 * 将层级容器列表转为数组对象
 * @param arr
 * @param show 是否显示新行
 * @returns
 */
function arrayToNestedObject(arr: ContainerOptionType[], show = true) {
  if (arr.length === 0) {
    return {};
  }
  let { name, type } = arr[arr.length - 1];
  const obj: NameMapValue = {};
  let target = arrayToNestedObject(arr.slice(0, -1), show);
  if (type === AttributeType.Array) {
    obj[name] = [];
    if (show) obj[name].push(target);
    return obj;
  }
  obj[name] = target;
  return obj;
}

/**
 * 合并相同对象数据
 * @param thisObj
 * @param lastObj
 * @param key
 * @returns
 */
function mergeObjects(
  thisObj: NameMapValue,
  lastObj: NameMapValue,
  key: string
) {
  if (
    !thisObj[key] ||
    !isArray(thisObj[key]) ||
    !lastObj[key] ||
    !isArray(lastObj[key])
  ) {
    return deepMerge({}, thisObj, lastObj);
  }
  let thisObjItem = thisObj[key][0];
  let lastObjItem = lastObj[key][0];
  return { [key]: [deepMerge({}, thisObjItem, lastObjItem)] };
}

// 合并对象属性
function deepMerge<T>(target: T, ...sources: Array<T>): T {
  if (!sources.length) return target;
  const source = sources.shift();
  if (target && _isObject(source)) {
    for (const key in source) {
      if (isArray(source[key])) {
        if (target && target[key]) {
          mergeObjects(target, source[key] as NameMapValue, key);
        } else {
          Object.assign(target as object, { [key]: source[key] });
        }
      } else if (isObject(source[key])) {
        if (!target[key]) Object.assign(target, { [key]: {} });
        deepMerge(target[key], source[key]);
      } else {
        Object.assign(target as object, { [key]: source[key] });
      }
    }
  }
  return deepMerge(target, ...sources);
}

/**
 * 添加属性
 * @param nestedObj 层级对象
 * @param pathParts 层级列表
 * @param propName 属性名
 * @param propValue 属性值
 */
function findAndAddProperty(
  tierObj: NameMapValue,
  tierList: ContainerOptionType[],
  propName: string,
  propValue: unknown
) {
  let currentObj = tierObj;
  for (let i = 0; i < tierList.length; i++) {
    const { name, type } = tierList[i];
    if (type === AttributeType.Array) {
      currentObj = currentObj[name][0];
    } else {
      if (hasOwn(currentObj, name)) {
        currentObj = currentObj[name];
      } else {
        throw new Error(`Path not found: ${name}`);
      }
    }
  }
  currentObj[propName] = propValue;
}

export function buildFormDataMap(params: DesFormParams) {
  let { widgetList, formConfig } = params;
  let containerList: ContainerMap = new Map([]);
  let containerKeysMap: ContainerWidgetNameMap = new Map([]);
  traverseFieldWidget({
    widgetList,
    formConfig,
    cb: (widget, parent) => {
      getContainerWidget(containerList)(widget, parent);
      buildDefaultValueListFn(containerList, containerKeysMap)(widget, parent);
    }
  });
  return containerKeysMap;
}

export function getFormDataTemplateList(params: DesFormParams) {
  let defaultValueList: string[] = [];
  let formDataMap = buildFormDataMap(params);
  for (let [key, value] of formDataMap.entries()) {
    if (key === FORM_OBJECT_KEY) {
      for (let name in value) {
        defaultValueList.push(`${name}:${JSON.stringify(value[name])},`);
      }
    } else {
      let propertyStr = objectToTemplateString(value[key]);
      defaultValueList.push(`${key}:${propertyStr},`);
    }
  }
  return defaultValueList;
}

export function getFormDataObject(params: DesFormParams, show = true) {
  let formData = {};
  openShowBlankRow = show;
  let formDataMap = buildFormDataMap(params);
  for (let value of formDataMap.values()) {
    Object.assign(formData, value);
  }
  openShowBlankRow = true;
  return formData;
}
