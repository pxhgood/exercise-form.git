/**
 * @description 组件属性构建
 */
import { deepClone, findPropertyPath } from "@exercise-form/utils";
import { DesFormWidgetParams } from "../interface";

const getAccept = (fileTypes: string[]) =>
  fileTypes.map((t: string) => "." + t).join(",");

// 获取当前属性所在对象位置的链式结构
export function getChainedName(
  chainedList: string[] | null,
  modelName: string
) {
  let list: string[] = [];
  let lastIndex = -1;
  let chainedName = "";
  if (chainedList) {
    chainedList.pop();
    lastIndex = chainedList.lastIndexOf("0");
    if (lastIndex !== -1) {
      // 上级为数组时，项命名为(${name}Item)
      let lastList = chainedList.slice(lastIndex + 1, chainedList.length);
      list.push(`${chainedList[lastIndex - 1]}Item`);
      list = list.concat(lastList);
      chainedName = `${list.join(".")}`;
    } else {
      list = chainedList;
      if (list.length) {
        chainedName = `${modelName}.${list.join(".")}`;
      } else {
        chainedName = modelName;
      }
    }
  }
  return chainedName;
}

export function getElAttr(params: DesFormWidgetParams) {
  let { widget, formConfig } = params;
  let wop = widget.options;
  let chainedList = findPropertyPath(
    formConfig[formConfig.modelName],
    wop.name
  );
  let deepChainedList = deepClone(chainedList);
  let modelValueName = getChainedName(chainedList, formConfig.modelName);
  return {
    chainedList: deepChainedList,
    modelValue: `v-model="${modelValueName}.${wop.name}"`,
    clearable: wop.clearable ? "clearable" : "",
    disabled: wop.disabled ? `disabled` : "",
    maxLength: wop.maxLength ? `:max-length="${wop.maxLength}"` : "",
    minLength: wop.minLength ? `:min-length="${wop.minLength}"` : "",
    multiple: wop.multiple ? "multiple" : "",
    multipleLimit: wop.multipleLimit
      ? `:multiple-limit="${wop.multipleLimit}"`
      : "",
    collapseTags: wop.collapseTags ? "collapse-tags" : "",
    collapseTagsTooltip: wop.collapseTagsTooltip ? "collapse-tags-tooltip" : "",
    maxCollapseTags:
      wop.maxCollapseTags > 1
        ? `:maxCollapseTags="${wop.maxCollapseTags}"`
        : "",
    placeholder: wop.placeholder ? `placeholder="${wop.placeholder}"` : "",
    readonly: wop.readonly ? `readonly` : "",
    required: wop.required ? `required` : "",
    showWordLimit: wop.showWordLimit ? "show-word-limit" : "",
    size: wop.size && wop.size !== "default" ? `size="${wop.size}"` : "",
    type: wop.type ? `type="${wop.type}"` : "",
    editable: wop.editable ? `editable` : "",
    border: wop.border ? `border` : "",
    allowHalf: wop.allowHalf ? `allow-half` : "",
    format: wop.format ? `format="${wop.format}"` : "",
    valueFormat: wop.valueFormat ? `value-format="${wop.valueFormat}"` : "",
    endPlaceholder: wop.endPlaceholder
      ? `end-placeholder="${wop.endPlaceholder}"`
      : "",
    startPlaceholder: wop.startPlaceholder
      ? `start-placeholder="${wop.startPlaceholder}"`
      : "",
    plain: wop.plain ? "plain" : "",
    link: wop.link ? "link" : "",
    round: wop.round ? "round" : "",
    circle: wop.circle ? "circle" : "",
    text: wop.text ? "text" : "",
    rows: wop.rows ? `:rows="${wop.rows}"` : "",
    max: wop.max ? `:max="${wop.max}"` : "",
    min: wop.min ? `:min="${wop.min}"` : "",
    step: wop.step ? `:step="${wop.step}"` : "",
    range: wop.range ? "range" : "",
    controlsPosition: wop.controlsPosition
      ? `controls-position="${wop.controlsPosition}"`
      : "",
    stepStrictly: wop.stepStrictly ? `step-strictly` : "",
    marks: wop.marks ? `:marks=${wop.marks}` : "",
    precision: wop.precision ? `:precision="${wop.precision}"` : "",
    controls: wop.controls ? "" : `:controls="${wop.controls}"`,
    showStops: wop.showStops ? `show-stops` : "",
    lowThreshold:
      wop.lowThreshold !== 2 ? `:low-threshold="${wop.lowThreshold}"` : "",
    highThreshold: wop.highThreshold
      ? `:high-threshold="${wop.highThreshold}"`
      : "",
    showText: wop.showText ? "show-text" : "",
    showScore: wop.showScore ? "show-score" : "",
    closable: wop.closable ? `closable` : "",
    description: wop.description ? `description="${wop.description}"` : "",
    showIcon: wop.showIcon ? `show-icon` : "",
    fontSize: wop.fontSize ? `style="{font-size:'${wop.fontSize}'}"` : "",
    tag: wop.tag ? `tag="${wop.tag}"` : "",
    truncated: wop.truncated ? `truncated` : "",
    borderStyle: wop.borderStyle ? `border-style="${wop.borderStyle}"` : "",
    contentPosition: wop.contentPosition
      ? `content-position="${wop.contentPosition}"`
      : "",
    direction: wop.direction ? `direction="${wop.direction}"` : "",
    title: wop.title ? `title="${wop.title}"` : "",
    labelWidth: wop.labelWidth ? `label-width="${wop.labelWidth}"` : "",
    align: wop.align ? `align="${wop.align}"` : "",
    label: wop.label ? `label="${wop.label}"` : "",
    prefixIcon: wop.prefixIcon ? `prefix-icon="${wop.prefixIcon}"` : "",
    suffixIcon: wop.suffixIcon ? `suffix-icon="${wop.suffixIcon}"` : "",
    //
    span: wop.span ? `:span="${wop.span}"` : "",
    md: wop.md ? `:md="${wop.md}"` : "",
    sm: wop.sm ? `:sm="${wop.sm}"` : "",
    xs: wop.xs ? `:xs="${wop.xs}"` : "",
    offset: wop.offset ? `:offset="${wop.offset}"` : "",
    push: wop.push ? `:push="${wop.push}"` : "",
    pull: wop.pull ? `:pull="${wop.pull}"` : "",
    colspan: wop.colspan > 1 ? `:colspan="${wop.colspan}"` : "",
    rowspan: wop.rowspan > 1 ? `:rowspan="${wop.rowspan}"` : "",

    shadow: wop.shadow ? `:shadow="true"` : "",
    cardWidth: wop.cardWidth ? `style="width:${wop.cardWidth};"` : "",
    tableWidth: wop.tableWidth ? `style="width:${wop.tableWidth};"` : "",
    showHeader: wop.showHeader ? "show-header" : "",
    fit: wop.fit ? "fit" : "",
    height: wop.height ? `height=${wop.height}` : "",
    highlightCurrentRow: wop.highlightCurrentRow ? `highlight-current-row` : "",
    operationWidth: wop.operationWidth ? `width="${wop.operationWidth}"` : "",
    operationLabel: wop.operationLabel ? `label="${wop.operationLabel}"` : "",
    operationFixed: wop.operationFixed ? `fixed="${wop.operationFixed}"` : "",
    operationAlign:
      wop.operationAlign && wop.operationAlign != "left"
        ? `align="${wop.operationAlign}"`
        : "",
    closeOnClickModal: !wop.closeOnClickModal
      ? `:close-on-click-modal="${wop.closeOnClickModal}`
      : "",
    closeOnPressEscape: !wop.closeOnPressEscape
      ? `:close-on-press-escape="${wop.closeOnPressEscape}"`
      : "",
    showClose: !wop.showClose ? `:show-close=${wop.showClose}` : "",
    modal: !wop.modal ? `:modal="${wop.modal}"` : "",

    activeIcon: wop.activeIcon ? `active-icon="${wop.activeIcon}"` : "",
    inactiveIcon: wop.inactiveIcon ? `inactive-icon="${wop.inactiveIcon}"` : "",
    activeActionIcon: wop.activeActionIcon
      ? `active-action-icon="${wop.activeActionIcon}"`
      : "",
    inactiveActionIcon: wop.inactiveActionIcon
      ? `inactive-action-icon="${wop.inactiveActionIcon}"`
      : "",
    activeText: wop.activeText ? `active-text="${wop.activeText}"` : "",
    inactiveText: wop.inactiveText ? `inactive-text="${wop.inactiveText}"` : "",

    width: wop.width ? `width=${wop.width}` : "",
    nodeKey: wop.nodeKey ? `node-key=${wop.nodeKey}` : "",
    draggable: wop.draggable ? "draggable" : "",
    defaultExpandAll: wop.defaultExpandAll ? "default-expand-all" : "",
    lazy: wop.lazy ? "lazy" : "",
    props: wop.props ? `:props=${wop.props}` : "",
    showLinkage: wop.showLinkage ? "show-linkage" : "",

    fileList: ` v-model:file-list="${wop.name}FileList"`,
    action: wop.action ? `action="${wop.action}"` : "",
    accept:
      wop.fileTypes && wop.fileTypes.length > 0
        ? `accept="${getAccept(wop.fileTypes)}"`
        : "",
    method: wop.method != "POST" ? `method="${wop.method}"` : "",
    limit: wop.limit ? `:limit="${wop.limit}"` : "",
    data: wop.data ? `:data="${wop.name}data"` : "",
    headers: wop.headers ? `:headers="${wop.name}headers"` : "",
    withCredentials: wop.withCredentials ? "with-credentials" : "",

    customClass:
      wop.customClass && wop.customClass.length > 0
        ? `class="${wop.customClass.join(" ")}"`
        : "",
    displayStyle: wop.displayStyle ? `style="display:${wop.displayStyle};"` : ""
  };
}
