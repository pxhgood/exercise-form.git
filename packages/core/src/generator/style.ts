/**
 * @description 存放默认导出的样式
 */

import {
  DesFormWidget,
  DesFormParams,
  DesFormWidgetStyleMethods
} from "../interface";
import { traverseFieldWidget } from "./vue3Js";
import { globalStyleProperty } from "./style-property";

const globalWidgetStyle = {
  grid: () => {
    return ``;
  },

  table: () => {
    return `
    table{
      width: 100%;
      table-layout: fixed;
      min-height: 40px;
      border-collapse: collapse;
      th,
      td {
        border: 1px solid;
        border-color: #dcdfe6;
      }
      td {
        height: 40px;
      }
    }`;
  },

  card: () => {
    return ``;
  },

  tabs: () => {
    return ``;
  },

  "data-table": (widget) => {
    let { showPagination, paginationAlign } = widget.options;
    let paginationAlignStyle = globalStyleProperty.justifyContentCenter;
    let paginationAlignClass = "";
    if (paginationAlign == "left")
      paginationAlignStyle = globalStyleProperty.justifyContentFlexStart;
    if (paginationAlign == "right")
      paginationAlignStyle = globalStyleProperty.justifyContentFlexEnd;
    if (showPagination) {
      paginationAlignClass = `:deep(.el-pagination){
        ${paginationAlignStyle}
      }`;
    }
    return `${paginationAlignClass}`;
  },

  "data-tree": () => {
    return ``;
  },

  "side-drawer": () => {
    return ``;
  },

  "popup-box": () => {
    return ``;
  },

  "sub-form": () => {
    return `.ex-sub-form-table {
      table-layout: fixed;
      min-height: 40px;
      border-collapse: collapse;
    
      th,
      td {
        border: 1px solid;
        border-color: #dcdfe6;
        padding: 10px;
        box-sizing: border-box;
      }
    
      th {
        height: 40px;
        font-weight: 400 !important;
        background-color: var(--el-fill-color-light);
      }
    
      td {
        height: 40px;
      }
    
      .row-on-column {
        width: 50px;
        display: flex;
        align-items: center;
        justify-content: center;
      }
    
      .row-on-header {
        width: 50px;
        display: flex;
        align-items: center;
        justify-content: center;
      }
    
      .row-on-index {
        margin-left: 10px;
      }
    
      .row-action-header {
        width: 120px;
        display: flex;
        align-items: center;
        justify-content: center;
    
        .column-btn {
          margin-left: 10px;
        }
      }
    
      .row-action-column {
        width: 120px;
        display: flex;
        align-items: center;
        justify-content: center;
      }
    
      .row-field-header,
      .row-field-column {
        display: flex;
        align-items: center;
        justify-content: center;
    
        .el-form-item {
          width: 100%;
        }
    
        .el-form-item__label {
          display: none !important;
        }
    
        .el-form-item--default {
          margin: 0 !important;
        }
      }
    
      .row-field-label-left {
        justify-content: flex-start;
      }
    
      .row-field-label-center {
        justify-content: center;
      }
    
      .row-field-label-right {
        justify-content: flex-end;
      }
    }`;
  },

  "grid-sub-form": () => {
    return `.ex-grid-sub-form {
      .row-header {
        display: flex;
        align-items: center;
        background-color: var(--el-fill-color-light);
        border: 1px solid;
        border-color: #dcdfe6;
        padding: 10px;
    
        .column-btn {
          margin-left: 10px;
        }
      }
    
      .row-column {
        display: flex;
        border-left: 1px solid;
        border-right: 1px solid;
        border-bottom: 1px solid;
        border-color: #dcdfe6;
      }
    
      .row-on-column {
        width: 50px;
        display: flex;
        align-items: center;
        justify-content: center;
        padding: 10px;
        box-sizing: border-box;
      }
    
      .row-on-index {
        margin-left: 10px;
      }
    
      .row-action-column {
        width: 120px;
        display: flex;
        align-items: center;
        justify-content: center;
        padding: 10px;
        box-sizing: border-box;
      }
    
      .row-field-column {
        flex: 1;
        border-left: 1px solid;
        border-color: #dcdfe6;
        padding: 10px;
        box-sizing: border-box;
      }
    
      .field-border-left {
        border-left: 1px solid;
        border-color: #dcdfe6;
      }
    
      .field-border-right {
        border-right: 1px solid;
        border-color: #dcdfe6;
      }
    }`;
  },

  input: () => {
    return ``;
  },

  "input-number": () => {
    return ``;
  },

  radio: () => {
    return ``;
  },

  checkbox: () => {
    return ``;
  },

  select: () => {
    return ``;
  },

  "date-picker": () => {
    return ``;
  },

  "time-picker": () => {
    return ``;
  },

  switch: () => {
    return ``;
  },

  cascader: () => {
    return ``;
  },

  rate: () => {
    return ``;
  },

  button: () => {
    return ``;
  },

  divider: () => {
    return ``;
  },

  slider: () => {
    return ``;
  },

  "color-picker": () => {
    return ``;
  },

  alert: () => {
    return ``;
  },

  text: () => {
    return ``;
  },

  slot: () => {
    return ``;
  },

  "upload-image": () => {
    return ``;
  },

  "upload-file": () => {
    return ``;
  },

  "rich-text": () => {
    return ``;
  }
} as DesFormWidgetStyleMethods;

function buildStyleTemplate(widget: DesFormWidget) {
  return globalWidgetStyle[widget.type] &&
    globalWidgetStyle[widget.type](widget)
    ? globalWidgetStyle[widget.type](widget)
    : "";
}

function buildStyleTemplateFn(styleList: string[], types: string[]) {
  return function (widget: DesFormWidget) {
    if (!types.includes(widget.type)) {
      let style = buildStyleTemplate(widget);
      styleList.push(style);
      types.push(widget.type);
    }
  };
}

export function getStyleTemplate(params: DesFormParams) {
  let { widgetList, formConfig } = params;
  let styleList: string[] = [];
  let widgetTypes: string[] = [];
  traverseFieldWidget({
    widgetList,
    formConfig,
    cb: (widget) => {
      buildStyleTemplateFn(styleList, widgetTypes)(widget);
    }
  });
  return `${styleList.join("\n")}`;
}
