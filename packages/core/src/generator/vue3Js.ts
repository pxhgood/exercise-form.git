import { DesFormWidget, DesFormParams } from "../interface";
import { getOuterTemplate } from "./outer";
import { getRegExp } from "@exercise-form/constants";
import { HAVE_DATA_KEYS } from "../config";
import { getFormDataTemplateList } from "./form-data";
import { getWidgetEventList } from "./event-method";

interface traverseWidgetParams extends DesFormParams {
  parent?: DesFormWidget;
  cb: (wt: DesFormWidget, parent?: DesFormWidget) => void;
}

export function traverseFieldWidget(params: traverseWidgetParams) {
  let { widgetList, formConfig, cb, parent } = params;
  widgetList.forEach((widget) => {
    cb(widget, parent);
    if (widget.category === "container") {
      if (widget.children) {
        traverseFieldWidget({
          widgetList: widget.children,
          formConfig,
          parent: widget,
          cb
        });
      }
    }
  });
}

function buildRulesListFn(rulesList: string[]) {
  return function (widget: DesFormWidget) {
    if (widget.category != "container") {
      let { required, validation, validationHint, name } = widget.options;
      if (required || validation) {
        let message = validationHint ? validationHint : "字段值不可为空";
        let requiredText = required
          ? `{required:true,
          message:"${message}"},`
          : "";
        let validationText = validation
          ? `{
            pattern:${getRegExp(validation)}, 
            trigger: ['blur', 'change'],
            message:"${message}"
          }`
          : "";
        let rulesText = `${name}:[${requiredText}${validationText}],`;
        rulesList.push(rulesText);
      }
    }
  };
}

function buildWidgetDataFn(dataParamsList: string[]) {
  return function (widget: DesFormWidget) {
    let options = widget.options;
    for (const key in options) {
      if (HAVE_DATA_KEYS.includes(key)) {
        dataParamsList.push(
          `const ${options.name}${key}=${JSON.stringify(options[key])}`
        );
      }
    }
    if (["upload-image", "upload-file"].includes(widget.type)) {
      dataParamsList.push(`const ${options.name}FileList=ref([])`);
    }
  };
}

function buildWidgetEventFn(eventFnList: string[], lifeCycleList: string[]) {
  return function (widget: DesFormWidget) {
    eventFnList.push(...getWidgetEventList(widget));
  };
}

export function genVue3JS(params: DesFormParams) {
  let { widgetList, formConfig } = params;
  let rulesList: string[] = [];
  let dataParamsList: string[] = [];
  let eventFnList: string[] = [];
  let lifeCycleList: string[] = [];
  let {
    formName,
    modelName,
    rulesName,
    onFormCreated,
    onFormMounted,
    functions
  } = formConfig;
  traverseFieldWidget({
    widgetList,
    formConfig,
    cb: (widget) => {
      buildRulesListFn(rulesList)(widget);
      buildWidgetDataFn(dataParamsList)(widget);
      buildWidgetEventFn(eventFnList, lifeCycleList)(widget);
    }
  });

  let fromDataTemplateList = getFormDataTemplateList(params);
  let { outerDefaultValueList } = getOuterTemplate({ widgetList, formConfig });

  let onBeforeMount = onFormCreated
    ? `onBeforeMount(()=>{${onFormCreated}})`
    : "";
  let onMounted = onFormMounted ? `onMounted(()=>{${onFormMounted}})` : "";

  let vue3JSTemplate = `import { ref, reactive } from 'vue';
  const ${formName}=ref();${
    formConfig.isPageType === "dialog" ? "const dialogVisible=ref(false);" : ""
  }
  ${outerDefaultValueList.join("\n")}
  const state = reactive({
    ${modelName}:{
      ${fromDataTemplateList.join("\n")}
    },
    ${rulesName}:{
      ${rulesList.join("\n")}
    }
  });
  const { ${modelName}, ${rulesName} } = state;
  ${dataParamsList.join("\n")}
  ${
    formConfig.isPageType === "dialog"
      ? "const open = (params) => {dialogVisible.value=true};const close=()=>{dialogVisible.value=false};"
      : ""
  }
  ${eventFnList.join("\n")}
  const handleSubmit = () => {
    if (!${formName}.value) return;
    ${formName}.value.validate((valid) => {
      if (valid) {
        console.log("submit!");
      } else {
        console.log("error submit!");
      }
    });
  };
  const handleReset = () => {
    if (!${formName}.value) return;
    ${formName}.value.resetFields();
  }
  ${functions}
  ${formConfig.isPageType === "dialog" ? "defineExpose({ open });" : ""};
  ${onBeforeMount}
  ${onMounted}
  `;
  return vue3JSTemplate;
}
