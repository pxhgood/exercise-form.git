import { makeInstaller } from "./installer";
import { ExCodeEditor } from "@exercise-form/components/code-editor";
import { ExFormDesigner } from "@exercise-form/components/form-designer";
import { ExFormRender } from "@exercise-form/components/form-render";
import {
  ExIconInstall,
  ExDesContainerInstall,
  ExDesFieldInstall,
  ExDesPropertyInstall,
  ExRenContainerInstall
} from "@exercise-form/widget";

import type { Plugin } from "vue";

const Components = [
  ExFormDesigner,
  ExFormRender,
  ExCodeEditor,
  ExDesContainerInstall.default,
  ExDesPropertyInstall.default,
  ExDesFieldInstall.default,
  ExRenContainerInstall.default,
  ExIconInstall.default
] as Plugin[];

export default makeInstaller([...Components]);
