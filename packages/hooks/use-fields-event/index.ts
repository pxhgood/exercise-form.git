import { inject } from "vue";
import {
  FormValidateCallback,
  TabsPaneContext,
  UploadRawFile,
  UploadFile,
  UploadFiles
} from "element-plus";
import { DesFormWidget } from "@exercise-form/core";
import { globalEventMethods } from "@exercise-form/core/src/generator/event-method";
import { formRenderRefKey } from "@exercise-form/components/form-render";
import type Node from "element-plus/es/components/tree/src/model/node";

export const useFields = (fieldWidget: DesFormWidget, formState = false) => {
  let fop = fieldWidget.options;
  const formRenderRef = inject(formRenderRefKey, undefined);

  const handleOnClick = (event: Event) => {
    if (fop.onClick && !formState) {
      let arg = globalEventMethods.onClick.params;
      new Function(arg, fop.onClick).call(formRenderRef!.value, event);
    }
  };

  const handleOnInput = (value: string | number) => {
    if (fop.onInput && !formState) {
      let arg = globalEventMethods.onInput.params;
      new Function(arg, fop.onInput).call(formRenderRef!.value, value);
    }
  };

  const handleOnChange = (value: any) => {
    if (fop.onChange && !formState) {
      let arg = globalEventMethods.onChange.params;
      new Function(arg, fop.onChange).call(formRenderRef!.value, value);
    }
  };

  const handleOnFocus = (event: Event) => {
    if (fop.onFocus && !formState) {
      let arg = globalEventMethods.onFocus.params;
      new Function(arg, fop.onFocus).call(formRenderRef!.value, event);
    }
  };

  const handleOnBlur = (event: Event) => {
    if (fop.onBlur && !formState) {
      let arg = globalEventMethods.onBlur.params;
      new Function(arg, fop.onBlur).call(formRenderRef!.value, event);
    }
  };

  const handleOnValidate = (
    rules: any,
    value: any,
    callback: FormValidateCallback
  ) => {
    if (fop.onValidate) {
      let arg = globalEventMethods.onValidate.params;
      new Function(arg, fop.onValidate).call(
        formRenderRef!.value,
        rules,
        value,
        callback
      );
    }
  };

  const handleOnClear = (event: Event) => {
    if (fop.onClear && !formState) {
      let arg = globalEventMethods.onClear.params;
      new Function(arg, fop.onClear).call(formRenderRef!.value, event);
    }
  };

  const handleOnRemoveTag = (tagValue: any) => {
    if (fop.onRemoveTag) {
      let arg = globalEventMethods.onRemoveTag.params;
      new Function(arg, fop.onRemoveTag).call(formRenderRef!.value, tagValue);
    }
  };

  const handleOnFrontAppendButtonClick = (event: Event) => {
    if (fop.onFrontAppendButtonClick) {
      let arg = globalEventMethods.onFrontAppendButtonClick.params;
      new Function(arg, fop.onFrontAppendButtonClick).call(
        formRenderRef!.value,
        event
      );
    }
  };

  const handleOnRearAppendButtonClick = (event: Event) => {
    if (fop.onRearAppendButtonClick) {
      let arg = globalEventMethods.onRearAppendButtonClick.params;
      new Function(arg, fop.onRearAppendButtonClick).call(
        formRenderRef!.value,
        event
      );
    }
  };

  // tabs
  const handleOnTabClick = (pane: TabsPaneContext, event: Event) => {
    if (fop.onTabClick) {
      let arg = globalEventMethods.onTabClick.params;
      new Function(arg, fop.onTabClick).call(formRenderRef!.value, pane, event);
    }
  };

  // tree
  const handleOnNodeClick = (data: any, node: Node, el: Event) => {
    if (fop.onNodeClick) {
      let arg = globalEventMethods.onNodeClick.params;
      new Function(arg, fop.onNodeClick).call(
        formRenderRef!.value,
        data,
        node,
        el
      );
    }
  };

  const handleOnNodeCheck = (data: any) => {
    if (fop.onNodeCheck) {
      let arg = globalEventMethods.onNodeCheck.params;
      new Function(arg, fop.onNodeCheck).call(formRenderRef!.value, data);
    }
  };

  const handleOnCheckChange = (node: Node) => {
    if (fop.onCheckChange) {
      let arg = globalEventMethods.onCheckChange.params;
      new Function(arg, fop.onCheckChange).call(formRenderRef!.value, node);
    }
  };

  // dialog or drawer
  const handleOnCancelButtonClick = (event: Event) => {
    if (fop.onCancelButtonClick) {
      let arg = globalEventMethods.onCancelButtonClick.params;
      new Function(arg, fop.onCancelButtonClick).call(
        formRenderRef!.value,
        event
      );
    }
  };

  const handleOnConfirmButtonClick = (event: Event) => {
    if (fop.onConfirmButtonClick) {
      let arg = globalEventMethods.onConfirmButtonClick.params;
      new Function(arg, fop.onConfirmButtonClick).call(
        formRenderRef!.value,
        event
      );
    }
  };

  //dialog
  const handleOnDialogClose = (event: Event) => {
    if (fop.onDialogClose) {
      let arg = globalEventMethods.onDialogClose.params;
      new Function(arg, fop.onDialogClose).call(formRenderRef!.value, event);
    }
  };

  const handleOnDialogOpened = (event: Event) => {
    if (fop.onDialogOpened) {
      let arg = globalEventMethods.onDialogOpened.params;
      new Function(arg, fop.onDialogOpened).call(formRenderRef!.value, event);
    }
  };

  // drawer
  const handleOnDrawerClose = (event: Event) => {
    if (fop.onDrawerClose) {
      let arg = globalEventMethods.onDrawerClose.params;
      new Function(arg, fop.onDrawerClose).call(formRenderRef!.value, event);
    }
  };

  const handleOnDrawerOpened = (event: Event) => {
    if (fop.onDrawerOpened) {
      let arg = globalEventMethods.onDrawerOpened.params;
      new Function(arg, fop.onDrawerOpened).call(formRenderRef!.value, event);
    }
  };

  //data-table
  const handleOnSelect = (selection: any[], row: any) => {
    if (fop.onSelect) {
      let arg = globalEventMethods.onSelect.params;
      new Function(arg, fop.onSelect).call(
        formRenderRef!.value,
        selection,
        row
      );
    }
  };

  const handleOnSelectAll = (selection: any[]) => {
    if (fop.onSelectAll) {
      let arg = globalEventMethods.onSelectAll.params;
      new Function(arg, fop.onSelectAll).call(formRenderRef!.value, selection);
    }
  };

  const handleOnSelectionChange = (newSelection: any[]) => {
    if (fop.onSelectionChange) {
      let arg = globalEventMethods.onSelectionChange.params;
      new Function(arg, fop.onSelectionChange).call(
        formRenderRef!.value,
        newSelection
      );
    }
  };

  const handleOnCellClick = (
    row: any,
    column: any,
    cell: any,
    event: Event
  ) => {
    if (fop.onCellClick) {
      let arg = globalEventMethods.onCellClick.params;
      new Function(arg, fop.onCellClick).call(
        formRenderRef!.value,
        row,
        column,
        cell,
        event
      );
    }
  };

  const handleOnRowClick = (row: any, column: any, event: Event) => {
    if (fop.onRowClick) {
      let arg = globalEventMethods.onRowClick.params;
      new Function(arg, fop.onRowClick).call(
        formRenderRef!.value,
        row,
        column,
        event
      );
    }
  };

  const handleOnHeaderClick = (event: Event) => {
    if (fop.onHeaderClick) {
      let arg = globalEventMethods.onHeaderClick.params;
      new Function(arg, fop.onHeaderClick).call(formRenderRef!.value, event);
    }
  };

  const handleOnSortChange = (column: any, prop: string, order: any) => {
    if (fop.onSortChange) {
      let arg = globalEventMethods.onSortChange.params;
      new Function(arg, fop.onSortChange).call(
        formRenderRef!.value,
        column,
        prop,
        order
      );
    }
  };

  const handleOnFilterChange = (newFilters: any) => {
    if (fop.onFilterChange) {
      let arg = globalEventMethods.onFilterChange.params;
      new Function(arg, fop.onFilterChange).call(
        formRenderRef!.value,
        newFilters
      );
    }
  };

  const handleOnExpandChange = (row: any, expandedRows: any[]) => {
    if (fop.onExpandChange) {
      let arg = globalEventMethods.onExpandChange.params;
      new Function(arg, fop.onExpandChange).call(
        formRenderRef!.value,
        row,
        expandedRows
      );
    }
  };

  const handleOnPageSizeChange = (value: number) => {
    if (fop.onPageSizeChange) {
      let arg = globalEventMethods.onPageSizeChange.params;
      new Function(arg, fop.onPageSizeChange)(value);
    }
  };

  const handleOnCurrentPageChange = (value: number) => {
    if (fop.onCurrentPageChange) {
      let arg = globalEventMethods.onCurrentPageChange.params;
      new Function(arg, fop.onCurrentPageChange).call(
        formRenderRef!.value,
        value
      );
    }
  };

  const onTableColumnClick = (row: any, index: number) => {
    if (fop.onTableColumnClick) {
      let arg = globalEventMethods.onTableColumnClick.params;
      new Function(arg, fop.onTableColumnClick).call(
        formRenderRef!.value,
        row,
        index
      );
    }
  };

  // sub-form
  const handleOnSubFormRowAdd = (parentData: object, widgetName: string) => {
    if (fop.onSubFormRowAdd) {
      let arg = globalEventMethods.onSubFormRowAdd.params;
      new Function(arg, fop.onSubFormRowAdd).call(
        formRenderRef!.value,
        parentData,
        widgetName
      );
    }
  };

  const handleOnSubFormRowInsert = (
    parentData: object,
    widgetName: string,
    index: number
  ) => {
    if (fop.onSubFormRowInsert) {
      let arg = globalEventMethods.onSubFormRowInsert.params;
      new Function(arg, fop.onSubFormRowInsert).call(
        formRenderRef!.value,
        parentData,
        widgetName,
        index
      );
    }
  };

  const handleOnSubFormRowDelete = (
    parentData: object,
    widgetName: string,
    index: number
  ) => {
    if (fop.onSubFormRowDelete) {
      let arg = globalEventMethods.onSubFormRowDelete.params;
      new Function(arg, fop.onSubFormRowDelete).call(
        formRenderRef!.value,
        parentData,
        widgetName,
        index
      );
    }
  };

  // upload file
  const handleOnBeforeUpload = (file: UploadRawFile) => {
    if (fop.onBeforeUpload && !formState) {
      let arg = globalEventMethods.onBeforeUpload.params;
      new Function(arg, fop.onBeforeUpload)(file);
    }
  };

  const handleOnSuccess = (
    result: any,
    file: UploadFile,
    fileList: UploadFiles
  ) => {
    if (fop.onSuccess && !formState) {
      let arg = globalEventMethods.onSuccess.params;
      new Function(arg, fop.onSuccess).call(
        formRenderRef!.value,
        result,
        file,
        fileList
      );
    }
  };

  const handleOnBeforeRemove = (file: UploadFile, fileList: UploadFiles) => {
    if (fop.onBeforeRemove && !formState) {
      let arg = globalEventMethods.onBeforeRemove.params;
      return new Function(arg, fop.onBeforeRemove).call(
        formRenderRef!.value,
        file,
        fileList
      ) as boolean;
    } else {
      return true;
    }
  };

  const handleOnRemove = (file: UploadFile, fileList: UploadFiles) => {
    if (fop.onRemove && !formState) {
      let arg = globalEventMethods.onRemove.params;
      new Function(arg, fop.onRemove).call(
        formRenderRef!.value,
        file,
        fileList
      );
    }
  };

  const handleOnError = (
    error: Error,
    file: UploadFile,
    fileList: UploadFiles
  ) => {
    if (fop.onError && !formState) {
      let arg = globalEventMethods.onError.params;
      new Function(arg, fop.onError).call(
        formRenderRef!.value,
        error,
        file,
        fileList
      );
    }
  };

  return {
    handleOnClick,
    handleOnInput,
    handleOnChange,
    handleOnFocus,
    handleOnBlur,
    handleOnValidate,
    handleOnClear,
    handleOnRemoveTag,
    handleOnFrontAppendButtonClick,
    handleOnRearAppendButtonClick,
    // tabs
    handleOnTabClick,
    // tree
    handleOnNodeClick,
    handleOnNodeCheck,
    handleOnCheckChange,
    // dialog or drawer
    handleOnCancelButtonClick,
    handleOnConfirmButtonClick,
    //dialog
    handleOnDialogClose,
    handleOnDialogOpened,
    // drawer
    handleOnDrawerClose,
    handleOnDrawerOpened,
    //data-table
    handleOnSelect,
    handleOnSelectAll,
    handleOnSelectionChange,
    handleOnCellClick,
    handleOnRowClick,
    handleOnHeaderClick,
    handleOnSortChange,
    handleOnFilterChange,
    handleOnExpandChange,
    handleOnPageSizeChange,
    handleOnCurrentPageChange,
    onTableColumnClick,
    // sub-form
    handleOnSubFormRowAdd,
    handleOnSubFormRowInsert,
    handleOnSubFormRowDelete,
    // upload file
    handleOnBeforeUpload,
    handleOnSuccess,
    handleOnBeforeRemove,
    handleOnRemove,
    handleOnError
  };
};
