import { ref, computed, watch } from "vue";
import { DesFormWidget } from "@exercise-form/core";
import { isEmptyObject } from "@exercise-form/utils";

export const useOptions = (
  widget: DesFormWidget,
  formData: { [key: string]: any } = {}
) => {
  const modelValue = ref(widget.options.modelDefaultValue);

  const label = computed(() => {
    let { optionsLabel } = widget.options;
    if (optionsLabel) return optionsLabel;
    return "label";
  });

  const value = computed(() => {
    let { optionsValue } = widget.options;
    if (optionsValue) return optionsValue;
    return "value";
  });

  const fieldModel = computed({
    get: () => {
      if (isEmptyObject(formData)) {
        return modelValue.value;
      } else {
        return formData[widget.options.name];
      }
    },
    set: (val) => {
      if (isEmptyObject(formData)) {
        modelValue.value = val;
      } else {
        formData[widget.options.name] = val;
      }
    }
  });

  watch(
    () => widget.options.modelDefaultValue,
    (val) => {
      modelValue.value = val;
    }
  );

  return {
    fieldModel,
    value,
    label
  };
};
