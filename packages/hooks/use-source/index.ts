import { ref, inject, onBeforeMount, onBeforeUnmount } from "vue";
import {
  DesFormWidget,
  DesFormSource,
  DesGlobalDvs,
  RequestBodyParams
} from "@exercise-form/core";
import {
  globalDvsKey,
  dataSourcesKey
} from "@exercise-form/components/form-render";
import $message from "@exercise-form/utils/message";
import { RequestCache } from "./request-cache";

const requestCache = new RequestCache();

const disposeRequestBodyParams = (
  list: RequestBodyParams[],
  DVS: DesGlobalDvs
) => {
  let params: { [key: string]: any } = {};
  list.forEach((item) => {
    let { key, value, type } = item;
    if (type === "number") {
      params[key] = Number(value);
    } else if (type === "boolean") {
      params[key] = value == "true" ? true : false;
    } else if (type === "variation") {
      let val = new Function("DVS", `return ${value}`)(DVS);
      params[key] = val;
    } else {
      params[key] = value;
    }
  });
  return params;
};

const handleResultData = (result: any, dataHandlerCode?: string) => {
  if (dataHandlerCode) return new Function("result", dataHandlerCode)(result);
  return result;
};

const handleError = (error: any, errorHandlerCode?: string) => {
  if (errorHandlerCode) {
    new Function("error", "$message", errorHandlerCode)(error, $message);
    return;
  }
  $message.onMessageError(error);
};

export const getRequestBodyParams = (
  sources: DesFormSource,
  DVS: DesGlobalDvs
) => {
  let { requestUrl, requestUrlType, headers, data, params } = sources;
  let url = requestUrl;
  if (requestUrlType === "variation" && requestUrl) {
    url = new Function("DVS", `return ${requestUrl}`)(DVS);
  }
  let reqHeaders = headers ? disposeRequestBodyParams(headers, DVS) : {};
  let reqData = data ? disposeRequestBodyParams(data, DVS) : {};
  let reqParams = params ? disposeRequestBodyParams(params, DVS) : {};

  return { url, headers: reqHeaders, data: reqData, params: reqParams };
};

export const requestApi = (
  sources: DesFormSource,
  dvs?: DesGlobalDvs
): Promise<any> => {
  return new Promise((resolve, reject) => {
    const globalDvs = inject(globalDvsKey, undefined);
    let { method, dataHandlerCode, errorHandlerCode } = sources;
    let DVS = dvs ? dvs : globalDvs ? globalDvs.value : {};
    let { url, headers, data, params } = getRequestBodyParams(sources, DVS);
    requestCache
      .fetch(url, {
        method,
        headers,
        data,
        params
      })
      .then((res) => {
        let data = handleResultData(res, dataHandlerCode);
        resolve(data);
      })
      .catch((error) => {
        handleError(error, errorHandlerCode);
        reject(error);
      });
  });
};

export const useSources = (widget: DesFormWidget, formState = false) => {
  const dataSources = inject(dataSourcesKey, undefined);
  const resultData = ref<any>(null);

  const getSourceData = () => {
    let {
      dataSourceName,
      showDataSource,
      dataSourceSetName,
      tableData,
      treeData,
      optionsItem
    } = widget.options;
    if (showDataSource && !formState) {
      let sources =
        dataSources &&
        dataSources.value!.find((item) => item.name == dataSourceName);
      sources &&
        requestApi(sources)
          .then((res) => {
            resultData.value = dataSourceSetName ? res[dataSourceSetName] : res;
          })
          .catch(() => {
            resultData.value = null;
          });
    } else {
      if (widget.type === "data-table") {
        resultData.value = tableData;
      } else if (widget.type === "data-tree") {
        resultData.value = treeData;
      } else if (["radio", "checkbox", "select"].includes(widget.type)) {
        resultData.value = optionsItem;
      } else {
        resultData.value = null;
      }
    }
  };

  const refreshData = () => {
    getSourceData();
  };

  onBeforeMount(() => {
    getSourceData();
  });

  onBeforeUnmount(() => {});

  return {
    resultData,
    refreshData
  };
};
