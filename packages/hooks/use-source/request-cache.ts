import axios from "axios";

export class RequestCache {
  private cache = new Map<string, Promise<any>>();
  private ttl = 5000;

  // 清除过期缓存项
  private clearExpired() {
    const now = Date.now();
    this.cache.forEach((promise, url) => {
      if ((promise as any).timestamp + this.ttl < now) {
        this.cache.delete(url);
      }
    });
  }

  // 清除当前
  clearCurrent(url: string) {
    this.cache.delete(url);
  }

  // 发送请求并缓存结果
  async fetch(url: string, options?: any) {
    this.clearExpired(); // 在每次请求前清除过期缓存

    if (this.cache.has(url)) {
      return this.cache.get(url);
    }
    const promise = axios(url, options)
      .then((res) => {
        return res;
      })
      .finally(() => {
        this.cache.delete(url);
      });

    // 添加时间戳到 Promise（用于过期检查）
    (promise as any).timestamp = Date.now();
    this.cache.set(url, promise);

    return promise;
  }
}
