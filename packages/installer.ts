import Draggable from "vuedraggable";
import type { App, Plugin } from "vue";
import { version } from "./version";

export const makeInstaller = (components: Plugin[] = []) => {
  const install = (app: App) => {
    components.forEach((c) => app.use(c));
    app.component("Draggable", Draggable);
  };
  return {
    install,
    version
  };
};
