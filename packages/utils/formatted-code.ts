import prettier from "prettier";
import parserHtml from "prettier/plugins/html";
import parserCss from "prettier/plugins/postcss";
import parserBabel from "prettier/plugins/babel";
import parserEstree from "prettier/plugins/estree";

// 格式化vue模板
export async function prettierCode(code: string) {
  try {
    return await prettier.format(code, {
      parser: "vue",
      plugins: [parserHtml, parserBabel, parserEstree, parserCss],
      tabWidth: 2,
      printWidth: 60,
      trailingComma: "none",
      htmlWhitespaceSensitivity: "ignore",
      vueIndentScriptAndStyle: true,
      singleAttributePerLine: true
    });
  } catch (error) {
    return code;
  }
}
