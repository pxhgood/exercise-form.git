import { isObject, isArray, hasOwn } from "./types";

type NestedObject = {
  [key: string]: any;
};

type CurrentPath = Array<string>;

export function isEmptyObject<T extends object>(obj: T): boolean {
  return Object.keys(obj).length === 0;
}

/**
 * 查询对象中属性所在位置
 * @param obj 目标对象
 * @param propertyName 目标属性
 * @returns
 */
export function findPropertyPath(
  obj: NestedObject,
  propertyName: string
): CurrentPath | null {
  function traverse(
    currentObj: NestedObject,
    currentPath: CurrentPath,
    targetName: string
  ): CurrentPath | null {
    for (const key in currentObj) {
      if (hasOwn(currentObj, key)) {
        const newPath = [...currentPath, key];
        const value = currentObj[key];
        if (key === targetName) {
          return newPath;
        } else if (isObject(currentObj[key])) {
          const result = traverse(value, newPath, targetName);
          if (result) {
            return result;
          }
        }
      }
    }
    return null;
  }

  return traverse(obj, [], propertyName);
}

// 将对象转换为模板字符串
export function objectToTemplateString(obj: object) {
  function convertObjectToTemplate(obj: object, indent = ""): string {
    if (isArray(obj)) {
      return `[${obj
        .map((item) => convertObjectToTemplate(item, indent + "  "))
        .join(",\n" + indent + "  ")}]`;
    } else if (isObject(obj)) {
      return `{${Object.entries(obj)
        .map(
          ([key, value]) =>
            `${indent}  ${key}: ${convertObjectToTemplate(
              value,
              indent + "  "
            )}`
        )
        .join(",\n" + indent + "  ")}}`;
    } else {
      return `${obj}`;
    }
  }
  return convertObjectToTemplate(obj);
}
