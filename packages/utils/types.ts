import { isArray } from "@vue/shared";

export {
  hasOwn,
  isArray,
  isObject,
  isString,
  isDate,
  isFunction,
  isPromise
} from "@vue/shared";

export const isNumber = (val: unknown) => typeof val === "number";

export const isEmptyArray = (val: unknown) => isArray(val) && val.length === 0;
