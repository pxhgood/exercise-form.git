export function deepClone<T>(value: T): T {
  if (!value) return value;
  if (Array.isArray(value))
    return value.map((item) => deepClone(item)) as unknown as T;
  if (value instanceof Date) return new Date(value) as unknown as T;
  if (typeof value === "object") {
    return Object.fromEntries(
      Object.entries(value).map(([k, v]: [string, any]) => {
        return [k, deepClone(v)];
      })
    ) as unknown as T;
  }
  return value;
}

// 生成uuid
export function getUniqueId() {
  return generateRandomString();
}

export function generateRandomString(length = 5): string {
  const characters = "abcdefghijklmnopqrstuvwxyz";
  let result = "";
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

// 生成随机数字
export function getRandomNumber() {
  let randomNumber;
  let generatedNumbers: Array<number> = [];
  do {
    randomNumber = Math.floor(Math.random() * 100000000);
  } while (
    generatedNumbers.includes(randomNumber) ||
    String(randomNumber).length !== 8
  );
  generatedNumbers.push(randomNumber);
  return randomNumber;
}

// 获取全局css
export function getGlobalCss(str: string) {
  let rules: Array<string> = str.match(/[^.]+(?={)|[^.]+(?=,)/g) ?? [];
  rules = rules.map((item) => item.trim());
  rules = Array.from(new Set([...rules]));
  return rules;
}

// 插入全局样式
export function insertGlobalStyle(style: string, id = "") {
  const head = document.head || document.getElementsByTagName("hade")[0];
  let oldStyleEl = document.getElementById("exercise-form-global-css");
  if (oldStyleEl) head.removeChild(oldStyleEl);
  if (id) {
    oldStyleEl = document.getElementById("exercise-form-global-css-" + id);
    if (oldStyleEl) head.removeChild(oldStyleEl);
  }

  const styleEl = document.createElement("style");
  styleEl.type = "text/css";
  styleEl.innerHTML = style;
  styleEl.id = id
    ? "exercise-form-global-css-" + id
    : "exercise-form-global-css";

  head.appendChild(styleEl);
}

// 插入全局函数
export function insertGlobalFunction(fnBody: string, id = "") {
  const body = document.body || document.getElementsByTagName("body")[0];
  let oldScript = document.getElementById("exercise-form-global-function");
  if (oldScript) body.removeChild(oldScript);
  if (id) {
    oldScript = document.getElementById("exercise-form-global-function-" + id);
    if (oldScript) body.removeChild(oldScript);
  }
  const scriptEl = document.createElement("script");
  scriptEl.type = "text/javascript";
  scriptEl.innerHTML = fnBody;
  scriptEl.id = id
    ? "exercise-form-global-function-" + id
    : "exercise-form-global-function";

  body.appendChild(scriptEl);
}
