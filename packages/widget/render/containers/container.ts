import { DesFormWidget } from "@exercise-form/core";
import { definePropType } from "@exercise-form/utils";

export const renContainerProps = {
  widgetData: {
    type: definePropType<DesFormWidget>(Object),
    default() {
      return {};
    }
  },
  formData: {
    type: definePropType<{ [key: string]: any }>(Object),
    default() {
      return {};
    }
  },
  parentData: {
    type: definePropType<DesFormWidget>(Object),
    default() {
      return {};
    }
  },
  subFormIndex: {
    type: Number
  },
  subFormPropList: {
    type: Array,
    default() {
      return [];
    }
  }
};
