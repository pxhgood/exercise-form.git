import { ElTree, ElTable } from "element-plus";

export interface TreeRef extends InstanceType<typeof ElTree> {
  handleExpandOrRetract: (value: boolean) => void;
  handleSelectAll: (value: boolean) => void;
  refreshData: () => void;
  getTreeData: () => any;
  setTreeData: (data: any) => void;
}

export interface TableRef extends InstanceType<typeof ElTable> {
  setPagination: (
    currentPage?: number,
    pageSize?: number,
    total?: number
  ) => void;
  refreshData: () => void;
  getTableData: () => any;
  setTableData: (data: any) => void;
  setTableColumnsHidden: (name: string, hidden: boolean) => void;
}
