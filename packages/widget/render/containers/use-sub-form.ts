import { computed, onMounted } from "vue";
import { DesFormWidget } from "@exercise-form/core";

export const useSubForm = (
  formData: { [key: string]: any },
  widget: DesFormWidget
) => {
  const isShow = computed(() => widget.options.columnPosition === "left");

  const subFormList = computed({
    get: () => formData[widget.options.name],
    set: (val) => val
  });

  const initSubFormList = () => {
    let { showBlankRow } = widget.options;
    if (showBlankRow && subFormList.value && subFormList.value.length == 0)
      subFormList.value.push({});
  };

  onMounted(() => {
    initSubFormList();
  });
  return {
    isShow,
    subFormList
  };
};
