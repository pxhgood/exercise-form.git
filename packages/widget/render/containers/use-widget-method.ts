import { inject, onBeforeMount, onMounted, Ref } from "vue";
import {
  globalFormDataKey,
  addWidgetRefFn,
  globalDvsKey
} from "@exercise-form/components/form-render";
import { DesFormWidget } from "@exercise-form/core";
import WidgetMethods from "./widget-method";

export const useRenderWidgetMethods = (
  fieldWidget: DesFormWidget,
  fieldsWidgetRef: Ref<any>
) => {
  const addWidgetRef = inject<addWidgetRefFn>("addWidgetRef");
  const globalDvs = inject(globalDvsKey, {} as any);
  const globalFormData = inject(globalFormDataKey, undefined);

  const initWidgetMethods = () => {
    const widgetRef = new WidgetMethods({
      fieldWidget,
      widgetRef: fieldsWidgetRef.value,
      globalDvs: globalDvs,
      globalFormData: globalFormData?.value || {}
    });

    let { onMounted, hidden } = fieldWidget.options;
    if (onMounted && !hidden) new Function(onMounted).call(widgetRef);

    if (addWidgetRef) {
      addWidgetRef({
        name: fieldWidget.options.name,
        widgetRef
      });
    }
  };

  onBeforeMount(() => {
    let { onCreated, hidden } = fieldWidget.options;
    if (onCreated && !hidden) new Function(onCreated)();
  });

  onMounted(() => {
    initWidgetMethods();
  });

  return {};
};
