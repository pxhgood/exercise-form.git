import {
  DesFormConfig,
  DesFormWidget,
  DesGlobalDvs
} from "@exercise-form/core";
import { getFormDataObject } from "@exercise-form/core/src/generator/form-data";
import {
  hasOwn,
  isEmptyObject,
  isArray,
  isString,
  isEmptyArray
} from "@exercise-form/utils";

type WidgetRefKey = { [key: string]: any };

interface WidgetMethodsOptions {
  fieldWidget: DesFormWidget;
  widgetRef: WidgetRefKey;
  globalDvs: DesGlobalDvs;
  globalFormData: object | undefined;
}

export default class WidgetMethods {
  fieldWidget: DesFormWidget;
  widgetRef: WidgetRefKey;
  globalDvs: DesGlobalDvs;
  globalFormData: object | undefined;
  constructor(options: WidgetMethodsOptions) {
    let { fieldWidget, widgetRef, globalDvs, globalFormData } = options;

    this.fieldWidget = fieldWidget;
    this.widgetRef = widgetRef;
    this.globalDvs = globalDvs;
    this.globalFormData = globalFormData;
  }

  getGlobalFormData() {
    return this.globalFormData;
  }

  getWidgetFormData() {
    let formConfig = {} as DesFormConfig;
    let widgetList = this.fieldWidget.children || [];
    let obj = getFormDataObject({
      widgetList,
      formConfig
    });
    return !isEmptyObject(obj) ? obj : null;
  }

  setHidden(value: boolean) {
    this.fieldWidget.options.hidden = value;
    this.widgetRef!.resetField();
  }

  setWidgetOptions(name: string, optionValue: any) {
    if (hasOwn(this.fieldWidget.options, name)) {
      this.fieldWidget.options[name] = optionValue;
    }
  }

  addCssClass(classValue: string[] | string) {
    if (isArray(classValue)) {
      let customClass = this.fieldWidget.options.customClass;
      this.fieldWidget.options.customClass = [
        ...new Set([...customClass, ...classValue])
      ];
    } else if (isString(classValue)) {
      this.fieldWidget.options.customClass.push(classValue);
    } else {
      console.error("The type should be array or string");
    }
  }

  removeCssClass(classValue: string[] | string) {
    if (!isEmptyArray(classValue) && classValue) {
      let customClass = this.fieldWidget.options.customClass as string[];
      let list = isArray(classValue) ? classValue : [classValue];
      this.fieldWidget.options.customClass = customClass.map(
        (c) => !list.includes(c)
      );
    }
  }

  getGlobalDsv() {
    return this.globalDvs;
  }

  // TODO：树形暂开、全选时，全选属性为更改
}
