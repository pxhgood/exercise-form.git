import path from "path";
import { writeFile } from "fs/promises";
import pkg from "../packages/exercise-form/package.json";
import { pkgPath } from "@exercise-form/build";

function getVersion() {
  return pkg.version;
}

const version = getVersion();

async function main() {
  console.info(`Version: ${version}`);
  await writeFile(
    path.resolve(pkgPath, "version.ts"),
    `export const version = "${version}";`
  );
}

main();
